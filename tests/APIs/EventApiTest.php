<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Admin\Event;

class EventApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_event()
    {
        $event = factory(Event::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/events', $event
        );

        $this->assertApiResponse($event);
    }

    /**
     * @test
     */
    public function test_read_event()
    {
        $event = factory(Event::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/events/'.$event->id
        );

        $this->assertApiResponse($event->toArray());
    }

    /**
     * @test
     */
    public function test_update_event()
    {
        $event = factory(Event::class)->create();
        $editedEvent = factory(Event::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/events/'.$event->id,
            $editedEvent
        );

        $this->assertApiResponse($editedEvent);
    }

    /**
     * @test
     */
    public function test_delete_event()
    {
        $event = factory(Event::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/events/'.$event->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/events/'.$event->id
        );

        $this->response->assertStatus(404);
    }
}
