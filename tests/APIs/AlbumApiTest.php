<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Admin\Album;

class AlbumApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_album()
    {
        $album = factory(Album::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/albums', $album
        );

        $this->assertApiResponse($album);
    }

    /**
     * @test
     */
    public function test_read_album()
    {
        $album = factory(Album::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/albums/'.$album->id
        );

        $this->assertApiResponse($album->toArray());
    }

    /**
     * @test
     */
    public function test_update_album()
    {
        $album = factory(Album::class)->create();
        $editedAlbum = factory(Album::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/albums/'.$album->id,
            $editedAlbum
        );

        $this->assertApiResponse($editedAlbum);
    }

    /**
     * @test
     */
    public function test_delete_album()
    {
        $album = factory(Album::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/albums/'.$album->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/albums/'.$album->id
        );

        $this->response->assertStatus(404);
    }
}
