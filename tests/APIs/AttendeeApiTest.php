<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Admin\Attendee;

class AttendeeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_attendee()
    {
        $attendee = factory(Attendee::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/attendees', $attendee
        );

        $this->assertApiResponse($attendee);
    }

    /**
     * @test
     */
    public function test_read_attendee()
    {
        $attendee = factory(Attendee::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/attendees/'.$attendee->id
        );

        $this->assertApiResponse($attendee->toArray());
    }

    /**
     * @test
     */
    public function test_update_attendee()
    {
        $attendee = factory(Attendee::class)->create();
        $editedAttendee = factory(Attendee::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/attendees/'.$attendee->id,
            $editedAttendee
        );

        $this->assertApiResponse($editedAttendee);
    }

    /**
     * @test
     */
    public function test_delete_attendee()
    {
        $attendee = factory(Attendee::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/attendees/'.$attendee->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/attendees/'.$attendee->id
        );

        $this->response->assertStatus(404);
    }
}
