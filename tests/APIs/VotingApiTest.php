<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Admin\Voting;

class VotingApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_voting()
    {
        $voting = factory(Voting::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/votings', $voting
        );

        $this->assertApiResponse($voting);
    }

    /**
     * @test
     */
    public function test_read_voting()
    {
        $voting = factory(Voting::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/votings/'.$voting->id
        );

        $this->assertApiResponse($voting->toArray());
    }

    /**
     * @test
     */
    public function test_update_voting()
    {
        $voting = factory(Voting::class)->create();
        $editedVoting = factory(Voting::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/votings/'.$voting->id,
            $editedVoting
        );

        $this->assertApiResponse($editedVoting);
    }

    /**
     * @test
     */
    public function test_delete_voting()
    {
        $voting = factory(Voting::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/votings/'.$voting->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/votings/'.$voting->id
        );

        $this->response->assertStatus(404);
    }
}
