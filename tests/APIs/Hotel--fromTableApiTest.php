<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Admin\Hotel--fromTable;

class Hotel--fromTableApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_hotel--from_table()
    {
        $hotelFromTable = factory(Hotel--fromTable::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/hotel--from_tables', $hotelFromTable
        );

        $this->assertApiResponse($hotelFromTable);
    }

    /**
     * @test
     */
    public function test_read_hotel--from_table()
    {
        $hotelFromTable = factory(Hotel--fromTable::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/hotel--from_tables/'.$hotelFromTable->id
        );

        $this->assertApiResponse($hotelFromTable->toArray());
    }

    /**
     * @test
     */
    public function test_update_hotel--from_table()
    {
        $hotelFromTable = factory(Hotel--fromTable::class)->create();
        $editedHotel--fromTable = factory(Hotel--fromTable::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/hotel--from_tables/'.$hotelFromTable->id,
            $editedHotel--fromTable
        );

        $this->assertApiResponse($editedHotel--fromTable);
    }

    /**
     * @test
     */
    public function test_delete_hotel--from_table()
    {
        $hotelFromTable = factory(Hotel--fromTable::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/hotel--from_tables/'.$hotelFromTable->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/hotel--from_tables/'.$hotelFromTable->id
        );

        $this->response->assertStatus(404);
    }
}
