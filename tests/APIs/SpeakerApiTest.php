<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Admin\Speaker;

class SpeakerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_speaker()
    {
        $speaker = factory(Speaker::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/speakers', $speaker
        );

        $this->assertApiResponse($speaker);
    }

    /**
     * @test
     */
    public function test_read_speaker()
    {
        $speaker = factory(Speaker::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/speakers/'.$speaker->id
        );

        $this->assertApiResponse($speaker->toArray());
    }

    /**
     * @test
     */
    public function test_update_speaker()
    {
        $speaker = factory(Speaker::class)->create();
        $editedSpeaker = factory(Speaker::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/speakers/'.$speaker->id,
            $editedSpeaker
        );

        $this->assertApiResponse($editedSpeaker);
    }

    /**
     * @test
     */
    public function test_delete_speaker()
    {
        $speaker = factory(Speaker::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/speakers/'.$speaker->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/speakers/'.$speaker->id
        );

        $this->response->assertStatus(404);
    }
}
