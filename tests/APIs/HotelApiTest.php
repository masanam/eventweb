<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Admin\Hotel;

class HotelApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_hotel()
    {
        $hotel = factory(Hotel::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/hotels', $hotel
        );

        $this->assertApiResponse($hotel);
    }

    /**
     * @test
     */
    public function test_read_hotel()
    {
        $hotel = factory(Hotel::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/hotels/'.$hotel->id
        );

        $this->assertApiResponse($hotel->toArray());
    }

    /**
     * @test
     */
    public function test_update_hotel()
    {
        $hotel = factory(Hotel::class)->create();
        $editedHotel = factory(Hotel::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/hotels/'.$hotel->id,
            $editedHotel
        );

        $this->assertApiResponse($editedHotel);
    }

    /**
     * @test
     */
    public function test_delete_hotel()
    {
        $hotel = factory(Hotel::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/hotels/'.$hotel->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/hotels/'.$hotel->id
        );

        $this->response->assertStatus(404);
    }
}
