<?php namespace Tests\Repositories;

use App\Models\Admin\Hotel--fromTable;
use App\Repositories\Admin\Hotel--fromTableRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Hotel--fromTableRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Hotel--fromTableRepository
     */
    protected $hotelFromTableRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->hotelFromTableRepo = \App::make(Hotel--fromTableRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_hotel--from_table()
    {
        $hotelFromTable = factory(Hotel--fromTable::class)->make()->toArray();

        $createdHotel--fromTable = $this->hotelFromTableRepo->create($hotelFromTable);

        $createdHotel--fromTable = $createdHotel--fromTable->toArray();
        $this->assertArrayHasKey('id', $createdHotel--fromTable);
        $this->assertNotNull($createdHotel--fromTable['id'], 'Created Hotel--fromTable must have id specified');
        $this->assertNotNull(Hotel--fromTable::find($createdHotel--fromTable['id']), 'Hotel--fromTable with given id must be in DB');
        $this->assertModelData($hotelFromTable, $createdHotel--fromTable);
    }

    /**
     * @test read
     */
    public function test_read_hotel--from_table()
    {
        $hotelFromTable = factory(Hotel--fromTable::class)->create();

        $dbHotel--fromTable = $this->hotelFromTableRepo->find($hotelFromTable->id);

        $dbHotel--fromTable = $dbHotel--fromTable->toArray();
        $this->assertModelData($hotelFromTable->toArray(), $dbHotel--fromTable);
    }

    /**
     * @test update
     */
    public function test_update_hotel--from_table()
    {
        $hotelFromTable = factory(Hotel--fromTable::class)->create();
        $fakeHotel--fromTable = factory(Hotel--fromTable::class)->make()->toArray();

        $updatedHotel--fromTable = $this->hotelFromTableRepo->update($fakeHotel--fromTable, $hotelFromTable->id);

        $this->assertModelData($fakeHotel--fromTable, $updatedHotel--fromTable->toArray());
        $dbHotel--fromTable = $this->hotelFromTableRepo->find($hotelFromTable->id);
        $this->assertModelData($fakeHotel--fromTable, $dbHotel--fromTable->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_hotel--from_table()
    {
        $hotelFromTable = factory(Hotel--fromTable::class)->create();

        $resp = $this->hotelFromTableRepo->delete($hotelFromTable->id);

        $this->assertTrue($resp);
        $this->assertNull(Hotel--fromTable::find($hotelFromTable->id), 'Hotel--fromTable should not exist in DB');
    }
}
