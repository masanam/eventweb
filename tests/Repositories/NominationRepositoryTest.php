<?php namespace Tests\Repositories;

use App\Models\Admin\Nomination;
use App\Repositories\Admin\NominationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class NominationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var NominationRepository
     */
    protected $nominationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->nominationRepo = \App::make(NominationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_nomination()
    {
        $nomination = factory(Nomination::class)->make()->toArray();

        $createdNomination = $this->nominationRepo->create($nomination);

        $createdNomination = $createdNomination->toArray();
        $this->assertArrayHasKey('id', $createdNomination);
        $this->assertNotNull($createdNomination['id'], 'Created Nomination must have id specified');
        $this->assertNotNull(Nomination::find($createdNomination['id']), 'Nomination with given id must be in DB');
        $this->assertModelData($nomination, $createdNomination);
    }

    /**
     * @test read
     */
    public function test_read_nomination()
    {
        $nomination = factory(Nomination::class)->create();

        $dbNomination = $this->nominationRepo->find($nomination->id);

        $dbNomination = $dbNomination->toArray();
        $this->assertModelData($nomination->toArray(), $dbNomination);
    }

    /**
     * @test update
     */
    public function test_update_nomination()
    {
        $nomination = factory(Nomination::class)->create();
        $fakeNomination = factory(Nomination::class)->make()->toArray();

        $updatedNomination = $this->nominationRepo->update($fakeNomination, $nomination->id);

        $this->assertModelData($fakeNomination, $updatedNomination->toArray());
        $dbNomination = $this->nominationRepo->find($nomination->id);
        $this->assertModelData($fakeNomination, $dbNomination->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_nomination()
    {
        $nomination = factory(Nomination::class)->create();

        $resp = $this->nominationRepo->delete($nomination->id);

        $this->assertTrue($resp);
        $this->assertNull(Nomination::find($nomination->id), 'Nomination should not exist in DB');
    }
}
