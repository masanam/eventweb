<?php namespace Tests\Repositories;

use App\Models\Admin\Hotel;
use App\Repositories\Admin\HotelRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HotelRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HotelRepository
     */
    protected $hotelRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->hotelRepo = \App::make(HotelRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_hotel()
    {
        $hotel = factory(Hotel::class)->make()->toArray();

        $createdHotel = $this->hotelRepo->create($hotel);

        $createdHotel = $createdHotel->toArray();
        $this->assertArrayHasKey('id', $createdHotel);
        $this->assertNotNull($createdHotel['id'], 'Created Hotel must have id specified');
        $this->assertNotNull(Hotel::find($createdHotel['id']), 'Hotel with given id must be in DB');
        $this->assertModelData($hotel, $createdHotel);
    }

    /**
     * @test read
     */
    public function test_read_hotel()
    {
        $hotel = factory(Hotel::class)->create();

        $dbHotel = $this->hotelRepo->find($hotel->id);

        $dbHotel = $dbHotel->toArray();
        $this->assertModelData($hotel->toArray(), $dbHotel);
    }

    /**
     * @test update
     */
    public function test_update_hotel()
    {
        $hotel = factory(Hotel::class)->create();
        $fakeHotel = factory(Hotel::class)->make()->toArray();

        $updatedHotel = $this->hotelRepo->update($fakeHotel, $hotel->id);

        $this->assertModelData($fakeHotel, $updatedHotel->toArray());
        $dbHotel = $this->hotelRepo->find($hotel->id);
        $this->assertModelData($fakeHotel, $dbHotel->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_hotel()
    {
        $hotel = factory(Hotel::class)->create();

        $resp = $this->hotelRepo->delete($hotel->id);

        $this->assertTrue($resp);
        $this->assertNull(Hotel::find($hotel->id), 'Hotel should not exist in DB');
    }
}
