<?php namespace Tests\Repositories;

use App\Models\Admin\Attendee;
use App\Repositories\Admin\AttendeeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AttendeeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AttendeeRepository
     */
    protected $attendeeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->attendeeRepo = \App::make(AttendeeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_attendee()
    {
        $attendee = factory(Attendee::class)->make()->toArray();

        $createdAttendee = $this->attendeeRepo->create($attendee);

        $createdAttendee = $createdAttendee->toArray();
        $this->assertArrayHasKey('id', $createdAttendee);
        $this->assertNotNull($createdAttendee['id'], 'Created Attendee must have id specified');
        $this->assertNotNull(Attendee::find($createdAttendee['id']), 'Attendee with given id must be in DB');
        $this->assertModelData($attendee, $createdAttendee);
    }

    /**
     * @test read
     */
    public function test_read_attendee()
    {
        $attendee = factory(Attendee::class)->create();

        $dbAttendee = $this->attendeeRepo->find($attendee->id);

        $dbAttendee = $dbAttendee->toArray();
        $this->assertModelData($attendee->toArray(), $dbAttendee);
    }

    /**
     * @test update
     */
    public function test_update_attendee()
    {
        $attendee = factory(Attendee::class)->create();
        $fakeAttendee = factory(Attendee::class)->make()->toArray();

        $updatedAttendee = $this->attendeeRepo->update($fakeAttendee, $attendee->id);

        $this->assertModelData($fakeAttendee, $updatedAttendee->toArray());
        $dbAttendee = $this->attendeeRepo->find($attendee->id);
        $this->assertModelData($fakeAttendee, $dbAttendee->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_attendee()
    {
        $attendee = factory(Attendee::class)->create();

        $resp = $this->attendeeRepo->delete($attendee->id);

        $this->assertTrue($resp);
        $this->assertNull(Attendee::find($attendee->id), 'Attendee should not exist in DB');
    }
}
