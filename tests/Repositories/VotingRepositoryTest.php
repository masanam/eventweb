<?php namespace Tests\Repositories;

use App\Models\Admin\Voting;
use App\Repositories\Admin\VotingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class VotingRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var VotingRepository
     */
    protected $votingRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->votingRepo = \App::make(VotingRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_voting()
    {
        $voting = factory(Voting::class)->make()->toArray();

        $createdVoting = $this->votingRepo->create($voting);

        $createdVoting = $createdVoting->toArray();
        $this->assertArrayHasKey('id', $createdVoting);
        $this->assertNotNull($createdVoting['id'], 'Created Voting must have id specified');
        $this->assertNotNull(Voting::find($createdVoting['id']), 'Voting with given id must be in DB');
        $this->assertModelData($voting, $createdVoting);
    }

    /**
     * @test read
     */
    public function test_read_voting()
    {
        $voting = factory(Voting::class)->create();

        $dbVoting = $this->votingRepo->find($voting->id);

        $dbVoting = $dbVoting->toArray();
        $this->assertModelData($voting->toArray(), $dbVoting);
    }

    /**
     * @test update
     */
    public function test_update_voting()
    {
        $voting = factory(Voting::class)->create();
        $fakeVoting = factory(Voting::class)->make()->toArray();

        $updatedVoting = $this->votingRepo->update($fakeVoting, $voting->id);

        $this->assertModelData($fakeVoting, $updatedVoting->toArray());
        $dbVoting = $this->votingRepo->find($voting->id);
        $this->assertModelData($fakeVoting, $dbVoting->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_voting()
    {
        $voting = factory(Voting::class)->create();

        $resp = $this->votingRepo->delete($voting->id);

        $this->assertTrue($resp);
        $this->assertNull(Voting::find($voting->id), 'Voting should not exist in DB');
    }
}
