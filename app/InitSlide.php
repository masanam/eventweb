<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InitSlide extends Model
{
    public function showData()
    {
      $store = [
        0 => [
          'menu' => 'Hotel',
          'icon' => 'mdi mdi-home-outline',
          'link'  => 'hotel',  
        ],
        1 => [
          'menu' => 'Agenda',
          'icon' => 'mdi mdi-open-in-app',
          'link'  => 'agenda',       
         ],
        2 => [
          'menu' => 'Speakers',
          'icon' => 'mdi mdi-laptop',
          'link'  => 'speaker',        
        ],
        3 => [
          'menu' => 'Panel Member',
          'icon' => 'mdi mdi-flask-outline',
          'link'  => 'member',        
        ],
        4 => [
          'menu' => 'Q&A',
          'icon' => 'mdi mdi-textbox',
          'link'  => 'faq',        
        ],
        5 => [
          'menu' => 'Live Polling',
          'icon' => 'mdi mdi-chart-line',
          'link'  => 'polling',       
         ],
        6 => [
          'menu' => 'Photos',
          'icon' => 'mdi mdi-shape-outline',
          'link'  => 'photo',        
        ],
        7 => [
          'menu' => 'Videos',
          'icon' => 'mdi mdi-access-point',
          'link'  => 'video',        
        ],
        8 => [
          'menu' => 'Survey',
          'icon' => 'mdi mdi-grid-large',
          'link'  => 'survey',        
        ],
        9 => [
          'menu' => 'PDFs',
          'icon' => 'mdi mdi-square-edit-outline',
          'link'  => 'pdf',        
        ],
        10 => [
          'menu' => 'Maps',
          'icon' => 'mdi mdi-cellphone-basic',
          'link'  => 'map',        
        ],
        
      ];

      return $store;
    }
}
