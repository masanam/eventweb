<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Nomination;
use App\Repositories\BaseRepository;

/**
 * Class NominationRepository
 * @package App\Repositories\Admin
 * @version February 11, 2020, 3:34 am UTC
*/

class NominationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'image',
        'business_name',
        'reason_for_nomination',
        'no_of_nominations',
        'is_admin_selected',
        'is_won',
        'user_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Nomination::class;
    }
}
