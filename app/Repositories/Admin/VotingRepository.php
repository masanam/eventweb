<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Voting;
use App\Repositories\BaseRepository;

/**
 * Class VotingRepository
 * @package App\Repositories\Admin
 * @version February 11, 2020, 3:35 am UTC
*/

class VotingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nomination_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Voting::class;
    }
}
