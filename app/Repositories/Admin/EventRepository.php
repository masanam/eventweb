<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Event;
use App\Repositories\BaseRepository;

/**
 * Class EventRepository
 * @package App\Repositories\Admin
 * @version February 7, 2020, 10:50 am UTC
*/

class EventRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'location',
        'description',
        'start_date',
        'end_date',
        'sale_date',
        'user_id',
        'currency_id',
        'sales_volume',
        'organiser_id',
        'venue_name',
        'venue_name_full',
        'location_address',
        'location_country',
        'location_state',
        'location_post_code',
        'location_lat',
        'location_long',
        'location_google_place_id',
        'pre_order_display_message',
        'post_order_display_message',
        'social_share_text',
        'social_show_facebook',
        'social_show_linkedin',
        'social_show_twitter',
        'social_show_email',
        'social_show_googleplus',
        'location_is_manual',
        'is_live',
        'barcode_type',
        'event_image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Event::class;
    }
}
