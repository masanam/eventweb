<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Speaker;
use App\Repositories\BaseRepository;

/**
 * Class SpeakerRepository
 * @package App\Repositories\Admin
 * @version February 7, 2020, 10:27 am UTC
*/

class SpeakerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'twitter',
        'facebook',
        'linkedin',
        'full_description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Speaker::class;
    }
}
