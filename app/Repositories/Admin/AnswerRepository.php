<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Response;
use App\Repositories\BaseRepository;

/**
 * Class AnswerRepository
 * @package App\Repositories\Admin
 * @version February 11, 2020, 3:34 am UTC
*/

class AnswerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'answer',
        'session_id',
        'question_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Response::class;
    }
}
