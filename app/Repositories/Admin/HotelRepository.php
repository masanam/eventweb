<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Hotel;
use App\Repositories\BaseRepository;

/**
 * Class HotelRepository
 * @package App\Repositories\Admin
 * @version February 17, 2020, 8:04 am UTC
*/

class HotelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address',
        'description',
        'rating',
        'facility',
        'resto',
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Hotel::class;
    }
}
