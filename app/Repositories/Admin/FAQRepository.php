<?php

namespace App\Repositories\Admin;

use App\Models\Admin\FAQ;
use App\Repositories\BaseRepository;

/**
 * Class FAQRepository
 * @package App\Repositories\Admin
 * @version February 7, 2020, 10:52 am UTC
*/

class FAQRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'question',
        'answer'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FAQ::class;
    }
}
