<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Attendee;
use App\Repositories\BaseRepository;

/**
 * Class AttendeeRepository
 * @package App\Repositories\Admin
 * @version February 7, 2020, 11:09 am UTC
*/

class AttendeeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'event_id',
        'ticket_id',
        'first_name',
        'last_name',
        'email',
        'private_reference_number',
        'is_cancelled',
        'has_arrived',
        'arrival_time',
        'account_id',
        'reference_index',
        'is_refunded'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Attendee::class;
    }
}
