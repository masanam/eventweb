<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InitMenu extends Model
{
    public function showData()
    {
      $store = [
        0 => [
          'menu'  => 'Social Feed',
          'image' => 'images/icons_contour/twitter.png',
          'link'  => 'socialfeed',
        ],
        1 => [
          'menu'  => 'Event Info',
          'image' => 'images/icons_contour/code.png',
          'link'  => 'eventinfo',
        ],
        2 => [
          'menu'  => 'Hotel',
          'image' => 'images/icons_contour/home.png',
          'link'  => 'hotel',
        ],
        3 => [
          'menu'  => 'Agenda',
          'image' => 'images/icons_contour/calendar.png',
          'link'  => 'agenda',
        ],
        4 => [
          'menu'  => 'Speaker',
          'image' => 'images/icons_contour/about.png',
          'link'  => 'speaker',
        ],
        5 => [
          'menu'  => 'Panel Member',
          'image' => 'images/icons_contour/clients.png',
          'link'  => 'member',
        ],
        6 => [
          'menu'  => 'Q&A',
          'image' => 'images/icons_contour/blog.png',
          'link'  => 'faq',
        ],
        7 => [
          'menu'  => 'Live Polling',
          'image' => 'images/icons_contour/stats.png',
          'link'  => 'polling',
        ],
        8 => [
          'menu'  => 'Photos',
          'image' => 'images/icons_contour/photos.png',
          'link'  => 'photo',
        ],
        9 => [
          'menu'  => 'Videos',
          'image' => 'images/icons_contour/videos.png',
          'link'  => 'video',
        ],
        10 => [
          'menu'  => 'Surveys',
          'image' => 'images/icons_contour/pencil.png',
          'link'  => 'survey',
        ],
        11 => [
          'menu'  => 'PDFs',
          'image' => 'images/icons_contour/docs.png',
          'link'  => 'pdf',
        ],
        12 => [
          'menu'  => 'Map',
          'image' => 'images/icons_contour/map_pin.png',
          'link'  => 'map',
        ],
        13 => [
          'menu'  => 'Ticket',
          'image' => 'images/icons_contour/code.png',
          'link'  => 'ticket',
        ],
        14 => [
          'menu'  => 'Sponsor',
          'image' => 'images/icons_contour/security.png',
          'link'  => 'sponsor',
        ],
      ];

      return $store;
    }
}
