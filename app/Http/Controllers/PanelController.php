<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\InitSlide;
use App\Models\Panel;

class PanelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $init_slide = new InitSlide();
        $slides = $init_slide->showData();
        $title = 'Create Panel';

        return view('pages.panels.create', compact('title','slides'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'location' => 'required',
            'description' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'account_id' => 'required',
            'user_id' => 'required',
            'seat_kuota' => 'required',
            'organiser_id' => 'required',
            // 'venue_name' => 'required',
            // 'location_address' => 'required',
            // 'location_lat' => 'required',
            // 'location_long' => 'required',
            // 'location_google_place_id' => 'required',
            // 'social_share_text' => 'required',
            // 'social_show_facebook' => 'required',
            // 'social_show_linkedin' => 'required',
            // 'social_show_twitter' => 'required',
            // 'social_show_email' => 'required',
            // 'social_show_google_plus' => 'required',
            // 'is_live' => 'required',
            // 'barcode_type' => 'required',
            // 'event_image' => 'required'
        ]);

        $data = $request->all();
        Panel::create($data);

        return redirect(url('member'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
