<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\InitSlide;
use App\InitMenu;
use App\Models\Admin\FAQ;


class PagesController extends Controller
{
  public function viewLanding()
  {

    $init_slide = new InitSlide();
    $slides = $init_slide->showData();

    $init_menu = new InitMenu();
    $menus = $init_menu->showData();

    $title = '';
    return view('pages.landing',compact('title','slides','menus'));
  }


  public function viewSocial()
  {
    $init_slide = new InitSlide();
    $slides = $init_slide->showData();
    $title = 'Social Feed';

    return view('pages.social',compact('title','slides'));
  }

  public function viewEvent()
  {
    $init_slide = new InitSlide();
    $slides = $init_slide->showData();
    $title = 'Event Info';

    $events = \App\Models\Admin\Event::all();
    $speakers = \App\Models\Admin\Speaker::all();


    return view('pages.eventinfo',compact('speakers','events','title','slides'));
  }

  public function viewMenu()
  {
    $init_slide = new InitSlide();
    $slides = $init_slide->showData();
    $title = 'Hotel';

    $hotels = \App\Models\Admin\Hotel::all();

    return view('pages.menus',compact('hotels','title','slides'));
  }

  public function viewAgenda()
  {
    $init_slide = new InitSlide();
    $slides = $init_slide->showData();
    $title = 'Agenda';

    $events = \App\Models\Admin\Event::first();

    $schedule1 = \App\Models\Admin\Schedule::with('speaker')->where('day_number',1)->get();
    $schedule2 = \App\Models\Admin\Schedule::with('speaker')->where('day_number',2)->get();


    return view('pages.agenda',compact('schedule1','schedule2','events','title','slides'));
  }

    public function viewSpeaker()
    {
      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'Speaker';
      $speakers = \App\Models\Admin\Speaker::all();

      return view('pages.speaker',compact('speakers','title','slides'));
    }

    public function viewMember()
    {
      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'Panel Member';
      $panels = \App\Models\Panel::with('panelMember')->get();

      return view('pages.member',compact('title','slides', 'panels'));
    }

    public function viewFaq()
    {

      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'FAQ';

      $faqs = \App\Models\Admin\FAQ::all();

      return view('pages.faqs',compact('faqs','title','slides'));
    }

    public function viewPolling()
    {
      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'Polling';

      return view('pages.polling',compact('title','slides'));
    }

    public function viewPhoto()
    {
      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'Photo';

      $photos = \App\Models\Admin\Gallery::where('album_id',1)->get();

      return view('pages.photo',compact('photos','title','slides'));
    }

    public function viewVideo()
    {
      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'Video';

      $videos = \App\Models\Admin\Gallery::where('album_id',2)->get();

      return view('pages.video',compact('videos','title','slides'));
    }

    public function viewSurvey()
    {
      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'Survey';
      $survey = \App\Models\Admin\Survey::with('questions')->with('responses')->get();

      foreach ($survey as $row){
        $questions = $row->questions;
      }
      // dd($survey);
      return view('pages.survey',compact('questions','survey','title','slides'));
    }

    public function viewPdf()
    {
      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'PDFs';

      $pdfs = \App\Models\Admin\Gallery::where('album_id', 3)->get();

      return view('pages.pdf',compact('title','slides','pdfs'));
    }

    public function viewMap()
    {
      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'Map';

      return view('pages.map',compact('title','slides'));
    }

    public function viewSponsor()
    {
      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'Sponsor';

      return view('pages.sponsor',compact('title','slides'));
    }

    public function viewTicket()
    {
      $init_slide = new InitSlide();
      $slides = $init_slide->showData();
      $title = 'Ticket';
  
      $tickets = \App\Models\Admin\Ticket::all();
  
      return view('pages.ticket',compact('tickets','title','slides'));
    }

    public function redirect($service) {
      return Socialite::driver ( $service )->redirect ();
   }

   public function callback($service) {
    $user = Socialite::with ( $service )->user ();

    return redirect('home')->withDetails ( $user )->withService ( $service );

  }

    
}
