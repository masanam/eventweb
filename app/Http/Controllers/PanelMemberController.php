<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\InitSlide;
use App\Models\Panel;
use App\Models\PanelMember;

class PanelMemberController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $init_slide = new InitSlide();
        $slides = $init_slide->showData();
        $title = 'Join';

        return view('pages.panelmembers.create', compact('title','slides'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'mobile_number' => 'required',
            'account_id' => 'required'
        ]);
        
        // Cek Data
        $panel = Panel::find($request->panel_id);
        $kuota = $panel->seat_kuota;
        $total = PanelMember::where('panel_id', $request->panel_id)->count();

        if($total >= $kuota){
            echo "kuota Penuh";
        }else{
            $data = $request->all();
            PanelMember::create($data);
            
            return redirect(url('member'));
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
