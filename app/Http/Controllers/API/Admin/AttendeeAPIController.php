<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateAttendeeAPIRequest;
use App\Http\Requests\API\Admin\UpdateAttendeeAPIRequest;
use App\Models\Admin\Attendee;
use App\Repositories\Admin\AttendeeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AttendeeController
 * @package App\Http\Controllers\API\Admin
 */

class AttendeeAPIController extends AppBaseController
{
    /** @var  AttendeeRepository */
    private $attendeeRepository;

    public function __construct(AttendeeRepository $attendeeRepo)
    {
        $this->attendeeRepository = $attendeeRepo;
    }

    /**
     * Display a listing of the Attendee.
     * GET|HEAD /attendees
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $attendees = $this->attendeeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($attendees->toArray(), 'Attendees retrieved successfully');
    }

    /**
     * Store a newly created Attendee in storage.
     * POST /attendees
     *
     * @param CreateAttendeeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAttendeeAPIRequest $request)
    {
        $input = $request->all();

        $attendee = $this->attendeeRepository->create($input);

        return $this->sendResponse($attendee->toArray(), 'Attendee saved successfully');
    }

    /**
     * Display the specified Attendee.
     * GET|HEAD /attendees/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Attendee $attendee */
        $attendee = $this->attendeeRepository->find($id);

        if (empty($attendee)) {
            return $this->sendError('Attendee not found');
        }

        return $this->sendResponse($attendee->toArray(), 'Attendee retrieved successfully');
    }

    /**
     * Update the specified Attendee in storage.
     * PUT/PATCH /attendees/{id}
     *
     * @param int $id
     * @param UpdateAttendeeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAttendeeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Attendee $attendee */
        $attendee = $this->attendeeRepository->find($id);

        if (empty($attendee)) {
            return $this->sendError('Attendee not found');
        }

        $attendee = $this->attendeeRepository->update($input, $id);

        return $this->sendResponse($attendee->toArray(), 'Attendee updated successfully');
    }

    /**
     * Remove the specified Attendee from storage.
     * DELETE /attendees/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Attendee $attendee */
        $attendee = $this->attendeeRepository->find($id);

        if (empty($attendee)) {
            return $this->sendError('Attendee not found');
        }

        $attendee->delete();

        return $this->sendSuccess('Attendee deleted successfully');
    }
}
