<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateVotingAPIRequest;
use App\Http\Requests\API\Admin\UpdateVotingAPIRequest;
use App\Models\Admin\Voting;
use App\Repositories\Admin\VotingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class VotingController
 * @package App\Http\Controllers\API\Admin
 */

class VotingAPIController extends AppBaseController
{
    /** @var  VotingRepository */
    private $votingRepository;

    public function __construct(VotingRepository $votingRepo)
    {
        $this->votingRepository = $votingRepo;
    }

    /**
     * Display a listing of the Voting.
     * GET|HEAD /votings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $votings = $this->votingRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($votings->toArray(), 'Votings retrieved successfully');
    }

    /**
     * Store a newly created Voting in storage.
     * POST /votings
     *
     * @param CreateVotingAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVotingAPIRequest $request)
    {
        $input = $request->all();

        $voting = $this->votingRepository->create($input);

        return $this->sendResponse($voting->toArray(), 'Voting saved successfully');
    }

    /**
     * Display the specified Voting.
     * GET|HEAD /votings/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Voting $voting */
        $voting = $this->votingRepository->find($id);

        if (empty($voting)) {
            return $this->sendError('Voting not found');
        }

        return $this->sendResponse($voting->toArray(), 'Voting retrieved successfully');
    }

    /**
     * Update the specified Voting in storage.
     * PUT/PATCH /votings/{id}
     *
     * @param int $id
     * @param UpdateVotingAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVotingAPIRequest $request)
    {
        $input = $request->all();

        /** @var Voting $voting */
        $voting = $this->votingRepository->find($id);

        if (empty($voting)) {
            return $this->sendError('Voting not found');
        }

        $voting = $this->votingRepository->update($input, $id);

        return $this->sendResponse($voting->toArray(), 'Voting updated successfully');
    }

    /**
     * Remove the specified Voting from storage.
     * DELETE /votings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Voting $voting */
        $voting = $this->votingRepository->find($id);

        if (empty($voting)) {
            return $this->sendError('Voting not found');
        }

        $voting->delete();

        return $this->sendSuccess('Voting deleted successfully');
    }
}
