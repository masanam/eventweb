<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateNominationAPIRequest;
use App\Http\Requests\API\Admin\UpdateNominationAPIRequest;
use App\Models\Admin\Nomination;
use App\Repositories\Admin\NominationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class NominationController
 * @package App\Http\Controllers\API\Admin
 */

class NominationAPIController extends AppBaseController
{
    /** @var  NominationRepository */
    private $nominationRepository;

    public function __construct(NominationRepository $nominationRepo)
    {
        $this->nominationRepository = $nominationRepo;
    }

    /**
     * Display a listing of the Nomination.
     * GET|HEAD /nominations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $nominations = $this->nominationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($nominations->toArray(), 'Nominations retrieved successfully');
    }

    /**
     * Store a newly created Nomination in storage.
     * POST /nominations
     *
     * @param CreateNominationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateNominationAPIRequest $request)
    {
        $input = $request->all();

        $nomination = $this->nominationRepository->create($input);

        return $this->sendResponse($nomination->toArray(), 'Nomination saved successfully');
    }

    /**
     * Display the specified Nomination.
     * GET|HEAD /nominations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Nomination $nomination */
        $nomination = $this->nominationRepository->find($id);

        if (empty($nomination)) {
            return $this->sendError('Nomination not found');
        }

        return $this->sendResponse($nomination->toArray(), 'Nomination retrieved successfully');
    }

    /**
     * Update the specified Nomination in storage.
     * PUT/PATCH /nominations/{id}
     *
     * @param int $id
     * @param UpdateNominationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNominationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Nomination $nomination */
        $nomination = $this->nominationRepository->find($id);

        if (empty($nomination)) {
            return $this->sendError('Nomination not found');
        }

        $nomination = $this->nominationRepository->update($input, $id);

        return $this->sendResponse($nomination->toArray(), 'Nomination updated successfully');
    }

    /**
     * Remove the specified Nomination from storage.
     * DELETE /nominations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Nomination $nomination */
        $nomination = $this->nominationRepository->find($id);

        if (empty($nomination)) {
            return $this->sendError('Nomination not found');
        }

        $nomination->delete();

        return $this->sendSuccess('Nomination deleted successfully');
    }
}
