<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\HotelDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateHotelRequest;
use App\Http\Requests\Admin\UpdateHotelRequest;
use App\Repositories\Admin\HotelRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class HotelController extends AppBaseController
{
    /** @var  HotelRepository */
    private $hotelRepository;

    public function __construct(HotelRepository $hotelRepo)
    {
        $this->hotelRepository = $hotelRepo;
    }

    /**
     * Display a listing of the Hotel.
     *
     * @param HotelDataTable $hotelDataTable
     * @return Response
     */
    public function index(HotelDataTable $hotelDataTable)
    {
        return $hotelDataTable->render('admin.hotels.index');
    }

    /**
     * Show the form for creating a new Hotel.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.hotels.create');
    }

    /**
     * Store a newly created Hotel in storage.
     *
     * @param CreateHotelRequest $request
     *
     * @return Response
     */
    public function store(CreateHotelRequest $request)
    {
        $input = $request->all();

        $hotel = $this->hotelRepository->create($input);

        Flash::success('Hotel saved successfully.');

        return redirect(route('admin.hotels.index'));
    }

    /**
     * Display the specified Hotel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $hotel = $this->hotelRepository->find($id);

        if (empty($hotel)) {
            Flash::error('Hotel not found');

            return redirect(route('admin.hotels.index'));
        }

        return view('admin.hotels.show')->with('hotel', $hotel);
    }

    /**
     * Show the form for editing the specified Hotel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $hotel = $this->hotelRepository->find($id);

        if (empty($hotel)) {
            Flash::error('Hotel not found');

            return redirect(route('admin.hotels.index'));
        }

        return view('admin.hotels.edit')->with('hotel', $hotel);
    }

    /**
     * Update the specified Hotel in storage.
     *
     * @param  int              $id
     * @param UpdateHotelRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHotelRequest $request)
    {
        $hotel = $this->hotelRepository->find($id);

        if (empty($hotel)) {
            Flash::error('Hotel not found');

            return redirect(route('admin.hotels.index'));
        }

        $hotel = $this->hotelRepository->update($request->all(), $id);

        Flash::success('Hotel updated successfully.');

        return redirect(route('admin.hotels.index'));
    }

    /**
     * Remove the specified Hotel from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $hotel = $this->hotelRepository->find($id);

        if (empty($hotel)) {
            Flash::error('Hotel not found');

            return redirect(route('admin.hotels.index'));
        }

        $this->hotelRepository->delete($id);

        Flash::success('Hotel deleted successfully.');

        return redirect(route('admin.hotels.index'));
    }
}
