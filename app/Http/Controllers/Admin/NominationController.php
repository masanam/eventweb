<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\NominationDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateNominationRequest;
use App\Http\Requests\Admin\UpdateNominationRequest;
use App\Repositories\Admin\NominationRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class NominationController extends AppBaseController
{
    /** @var  NominationRepository */
    private $nominationRepository;

    public function __construct(NominationRepository $nominationRepo)
    {
        $this->nominationRepository = $nominationRepo;
    }

    /**
     * Display a listing of the Nomination.
     *
     * @param NominationDataTable $nominationDataTable
     * @return Response
     */
    public function index(NominationDataTable $nominationDataTable)
    {
        return $nominationDataTable->render('admin.nominations.index');
    }

    /**
     * Show the form for creating a new Nomination.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.nominations.create');
    }

    /**
     * Store a newly created Nomination in storage.
     *
     * @param CreateNominationRequest $request
     *
     * @return Response
     */
    public function store(CreateNominationRequest $request)
    {
        $input = $request->all();

        $nomination = $this->nominationRepository->create($input);

        Flash::success('Nomination saved successfully.');

        return redirect(route('admin.nominations.index'));
    }

    /**
     * Display the specified Nomination.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $nomination = $this->nominationRepository->find($id);

        if (empty($nomination)) {
            Flash::error('Nomination not found');

            return redirect(route('admin.nominations.index'));
        }

        return view('admin.nominations.show')->with('nomination', $nomination);
    }

    /**
     * Show the form for editing the specified Nomination.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $nomination = $this->nominationRepository->find($id);

        if (empty($nomination)) {
            Flash::error('Nomination not found');

            return redirect(route('admin.nominations.index'));
        }

        return view('admin.nominations.edit')->with('nomination', $nomination);
    }

    /**
     * Update the specified Nomination in storage.
     *
     * @param  int              $id
     * @param UpdateNominationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNominationRequest $request)
    {
        $nomination = $this->nominationRepository->find($id);

        if (empty($nomination)) {
            Flash::error('Nomination not found');

            return redirect(route('admin.nominations.index'));
        }

        $nomination = $this->nominationRepository->update($request->all(), $id);

        Flash::success('Nomination updated successfully.');

        return redirect(route('admin.nominations.index'));
    }

    /**
     * Remove the specified Nomination from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $nomination = $this->nominationRepository->find($id);

        if (empty($nomination)) {
            Flash::error('Nomination not found');

            return redirect(route('admin.nominations.index'));
        }

        $this->nominationRepository->delete($id);

        Flash::success('Nomination deleted successfully.');

        return redirect(route('admin.nominations.index'));
    }
}
