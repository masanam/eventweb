<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Schedule
 * @package App\Models\Admin
 * @version February 7, 2020, 10:33 am UTC
 *
 * @property integer day_number
 * @property time start_time
 * @property string title
 * @property string subtitle
 * @property integer speaker_id
 */
class Schedule extends Model
{
    use SoftDeletes;

    public $table = 'schedules';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'day_number',
        'start_time',
        'title',
        'subtitle',
        'event_id',
        'speaker_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'day_number' => 'integer',
        'title' => 'string',
        'subtitle' => 'string',
        'speaker_id' => 'integer',
        'event_id' => 'integer'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'day_number' => 'required',
        'start_time' => 'required',
        'title' => 'required'
    ];

    
    public function speaker()
    {
        return $this->belongsTo(\App\Models\Admin\Speaker::class, 'speaker_id', 'id');
    }
    
    
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    
}
