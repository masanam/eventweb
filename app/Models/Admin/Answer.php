<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Answer
 * @package App\Models\Admin
 * @version February 11, 2020, 3:34 am UTC
 *
 * @property string answer
 * @property integer user_id
 * @property integer question_id
 */
class Answer extends Model
{
    use SoftDeletes;

    public $table = 'answers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'answer',
        'user_id',
        'question_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'answer' => 'string',
        'user_id' => 'integer',
        'question_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'answer' => 'required',
        'user_id' => 'required',
        'question_id' => 'required'
    ];

    
}
