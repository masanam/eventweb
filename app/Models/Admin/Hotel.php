<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Hotel
 * @package App\Models\Admin
 * @version February 17, 2020, 8:04 am UTC
 *
 * @property string name
 * @property string address
 * @property string description
 * @property integer rating
 * @property string facility
 * @property string resto
 * @property string image
 */
class Hotel extends Model
{
    use SoftDeletes;

    public $table = 'hotels';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'address',
        'description',
        'rating',
        'facility',
        'resto',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'description' => 'string',
        'rating' => 'integer',
        'facility' => 'string',
        'resto' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
