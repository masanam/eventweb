<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Speaker
 * @package App\Models\Admin
 * @version February 7, 2020, 10:27 am UTC
 *
 * @property string name
 * @property string description
 * @property string twitter
 * @property string facebook
 * @property string linkedin
 * @property string full_description
 */
class Speaker extends Model
{
    use SoftDeletes;

    public $table = 'speakers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description',
        'twitter',
        'facebook',
        'linkedin',
        'full_description',
        'event_id',
        'photo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'twitter' => 'string',
        'facebook' => 'string',
        'linkedin' => 'string',
        'full_description' => 'string',
        'photo' => 'string'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
