<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Album
 * @package App\Models\Admin
 * @version February 20, 2020, 6:59 am UTC
 *
 * @property string title
 * @property string seotitle
 * @property string active
 * @property integer created_by
 * @property integer updated_by
 */
class Album extends Model
{
    use SoftDeletes;

    public $table = 'albums';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'seotitle',
        'active',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'seotitle' => 'string',
        'active' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'active' => 'required',
        'created_by' => 'required',
        'updated_by' => 'required'
    ];

    
}
