<?php

namespace App\Models\Admin;

// use Illuminate\Database\Eloquent\Collection;
// use Illuminate\Database\Eloquent\Model;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\Question;

class Response extends Model
{
    protected $fillable = ['session_id', 'question_id', 'answer'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'question' => 'required'
    ];


    /**
     * Get the question whose response this is
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
