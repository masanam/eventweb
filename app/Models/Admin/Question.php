<?php

namespace App\Models\Admin;

// use Illuminate\Database\Eloquent\Collection;
// use Illuminate\Database\Eloquent\Model;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\Response;
use App\Models\Admin\Survey;


/**
 * @property mixed id
 */
class Question extends Model
{
    // Question type constants
    const FREE_TEXT = 'free_text';
    const NUMBERS = 'numbers';
    const SINGLE_ANSWER = 'single_answer';
    const MULTIPLE_ANSWER = 'multiple_answer';

    protected $fillable = ['question', 'type', 'options', 'survey_id'];

    protected $casts = [
        'options' => 'array'
    ];

    /**
     * Get the survey this question belongs to
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }


    /**
     * Get all responses for this question
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function responses()
    {
        return $this->hasMany(Response::class);
    }

        /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'question' => 'required'
    ];

    /**
     * Get answer stats for this question
     * @return \Illuminate\Support\Collection
     */
    public function getResponseStatsAttribute()
    {
        return DB::table('responses')
            ->selectRaw('answer, count(answer) as answer_count')
            ->where('question_id', $this->id)
            ->groupBy('answer')
            ->get();
    }
}
