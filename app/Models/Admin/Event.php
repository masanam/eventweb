<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Event
 * @package App\Models\Admin
 * @version February 7, 2020, 10:50 am UTC
 *
 * @property string title
 * @property string location
 * @property string description
 * @property string|\Carbon\Carbon start_date
 * @property string|\Carbon\Carbon end_date
 * @property string|\Carbon\Carbon sale_date
 * @property integer user_id
 * @property integer currency_id
 * @property integer sales_volume
 * @property integer organiser_id
 * @property string venue_name
 * @property string venue_name_full
 * @property string location_address
 * @property string location_country
 * @property string location_state
 * @property string location_post_code
 * @property string location_lat
 * @property string location_long
 * @property string location_google_place_id
 * @property string pre_order_display_message
 * @property string post_order_display_message
 * @property string social_share_text
 * @property boolean social_show_facebook
 * @property boolean social_show_linkedin
 * @property boolean social_show_twitter
 * @property boolean social_show_email
 * @property boolean social_show_googleplus
 * @property integer location_is_manual
 * @property boolean is_live
 * @property string barcode_type
 * @property string event_image
 */
class Event extends Model
{
    use SoftDeletes;

    public $table = 'events';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'location',
        'description',
        'start_date',
        'end_date',
        'sale_date',
        'user_id',
        'currency_id',
        'sales_volume',
        'organiser_id',
        'venue_name',
        'venue_name_full',
        'location_address',
        'location_country',
        'location_state',
        'location_post_code',
        'location_lat',
        'location_long',
        'location_google_place_id',
        'pre_order_display_message',
        'post_order_display_message',
        'social_share_text',
        'social_show_facebook',
        'social_show_linkedin',
        'social_show_twitter',
        'social_show_email',
        'social_show_googleplus',
        'location_is_manual',
        'is_live',
        'barcode_type',
        'event_image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'location' => 'string',
        'description' => 'string',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'sale_date' => 'datetime',
        'user_id' => 'integer',
        'currency_id' => 'integer',
        'sales_volume' => 'integer',
        'organiser_id' => 'integer',
        'venue_name' => 'string',
        'venue_name_full' => 'string',
        'location_address' => 'string',
        'location_country' => 'string',
        'location_state' => 'string',
        'location_post_code' => 'string',
        'location_lat' => 'string',
        'location_long' => 'string',
        'location_google_place_id' => 'string',
        'pre_order_display_message' => 'string',
        'post_order_display_message' => 'string',
        'social_share_text' => 'string',
        'social_show_facebook' => 'boolean',
        'social_show_linkedin' => 'boolean',
        'social_show_twitter' => 'boolean',
        'social_show_email' => 'boolean',
        'social_show_googleplus' => 'boolean',
        'location_is_manual' => 'integer',
        'is_live' => 'boolean',
        'barcode_type' => 'string',
        'event_image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'description' => 'required'
    ];

    
}
