<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Gallery
 * @package App\Models\Admin
 * @version February 20, 2020, 7:03 am UTC
 *
 * @property integer album_id
 * @property string title
 * @property string content
 * @property string picture
 * @property integer created_by
 * @property integer updated_by
 */
class Gallery extends Model
{
    use SoftDeletes;

    public $table = 'galleries';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'album_id',
        'title',
        'content',
        'picture',
        'links',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'album_id' => 'integer',
        'title' => 'string',
        'content' => 'string',
        'picture' => 'string',
        'links' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'album_id' => 'integer',

    ];

    public function album()
    {
        return $this->belongsTo(\App\Models\Admin\Album::class, 'album_id', 'id');
    }

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
    
}
