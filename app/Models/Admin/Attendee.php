<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Attendee
 * @package App\Models\Admin
 * @version February 7, 2020, 11:09 am UTC
 *
 * @property integer order_id
 * @property integer event_id
 * @property integer ticket_id
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string private_reference_number
 * @property boolean is_cancelled
 * @property boolean has_arrived
 * @property string|\Carbon\Carbon arrival_time
 * @property integer account_id
 * @property integer reference_index
 * @property boolean is_refunded
 */
class Attendee extends Model
{
    use SoftDeletes;

    public $table = 'attendees';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'order_id',
        'event_id',
        'ticket_id',
        'first_name',
        'last_name',
        'email',
        'private_reference_number',
        'is_cancelled',
        'has_arrived',
        'arrival_time',
        'account_id',
        'reference_index',
        'is_refunded'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'event_id' => 'integer',
        'ticket_id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'private_reference_number' => 'string',
        'is_cancelled' => 'boolean',
        'has_arrived' => 'boolean',
        'arrival_time' => 'datetime',
        'account_id' => 'integer',
        'reference_index' => 'integer',
        'is_refunded' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'order_id' => 'required',
        'event_id' => 'required',
        'ticket_id' => 'required',
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required',
        'private_reference_number' => 'required',
        'is_cancelled' => 'required',
        'has_arrived' => 'required',
        'account_id' => 'required',
        'reference_index' => 'required',
        'is_refunded' => 'required'
    ];

    
}
