<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;

    public $table = 'members';

    protected $fillable = [
        'order_id', 
        'event_id', 
        'ticket_id', 
        'first_name',
        'last_name',
        'email',
        'private_reference_number',
        'account_id',
        'reference_index'
    ];

    protected $casts = [
        'order_id' => 'integer', 
        'event_id' => 'integer', 
        'ticket_id' => 'integer', 
        'first_name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'private_reference_number' => 'integer',
        'account_id' => 'integer',
        'reference_index' => 'integer'
    ];
}
