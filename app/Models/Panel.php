<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Panel extends Model
{
    use SoftDeletes;

    protected $table = 'panels';

    protected $fillable = [
        'title',
        'location',
        'description',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'account_id',
        'user_id',
        'seat_kuota',
        'organiser_id',
        'venue_name',
        'location_address',
        'location_lat',
        'location_long',
        'location_google_place_id',
        'social_share_text',
        'social_show_facebook',
        'social_show_linkedin',
        'social_show_twitter',
        'social_show_email',
        'social_show_google_plus',
        'is_live',
        'barcode_type',
        'event_image'
    ];

    protected $casts = [
        'title' => 'string',
        'location' => 'string',
        'account_id' => 'integer',
        'user_id' => 'integer',
        'seat_kuota' => 'integer',
        'organiser_id' => 'integer',
        'venue_name' => 'string',
        'location_address' => 'string',
        'location_lat' => 'string',
        'location_long' => 'string',
        'location_google_place_id' => 'string',
        'social_show_facebook' => 'integer',
        'social_show_linkedin' => 'integer',
        'social_show_twitter' => 'integer',
        'social_show_email' => 'integer',
        'social_show_google_plus' => 'integer',
        'is_live' => 'integer',
    ];

    public function panelMember()
    {
        return $this->hasMany(PanelMember::class, 'panel_id')->orderBy('id', 'DESC');
    }
}
