<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PanelMember extends Model
{
    use SoftDeletes;

    protected $table = 'panelmembers';

    protected $fillable = [
        'panel_id',
        'first_name',
        'last_name',
        'email',
        'mobile_number',
        'account_id'
    ];

    protected $casts = [
        'panel_id' => 'integer',
        'first_name' => 'string',
        'last_name'=> 'string',
        'email' => 'string',
        'mobile_number' => 'string',
        'account_id' => 'integer'
    ];


    public function panel()
    {
        return $this->belongsTo(Panel::class, 'panel_id', 'id');
    }
}
