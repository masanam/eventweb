<!-- Question Field -->
<div class="form-group col-sm-6">
    {!! Form::label('question', 'Question:') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    @if (isset($question))
    <select id="type" name="type" class="form-control">
        <option value="free_text" {{ ( $question->type == 'free_text' ) ? 'selected' : '' }} >Free Text</option>
        <option value="numbers" {{ ( $question->type == 'numbers' ) ? 'selected' : '' }} >Numbers</option>
        <option value="single_answer" {{ ( $question->type == 'single_answer' ) ? 'selected' : '' }} >Single Answer</option>
        <option value="multiple_answer" {{ ( $question->type == 'multiple_answer' ) ? 'selected' : '' }} >Multiple Answer</option>
    </select>
    @else
    <select id="type" name="type" class="form-control">
        <option value="free_text">Free Text</option>
        <option value="numbers">Numbers</option>
        <option value="single_answer">Single Answer</option>
        <option value="multiple_answer">Multiple Answer</option>
    </select>
    @endif
</div>

<!-- Options Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('options', 'Options:') !!}
    {!! Form::textarea('options', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('survey_id', 'Survey Id:') !!}
    {!! Form::text('survey_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.questions.index') }}" class="btn btn-secondary">Cancel</a>
</div>
