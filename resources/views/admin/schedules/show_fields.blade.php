<!-- Day Number Field -->
<div class="form-group">
    {!! Form::label('day_number', 'Day Number:') !!}
    <p>{{ $schedule->day_number }}</p>
</div>

<!-- Start Time Field -->
<div class="form-group">
    {!! Form::label('start_time', 'Start Time:') !!}
    <p>{{ $schedule->start_time }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $schedule->title }}</p>
</div>

<!-- Subtitle Field -->
<div class="form-group">
    {!! Form::label('subtitle', 'Subtitle:') !!}
    <p>{{ $schedule->subtitle }}</p>
</div>

<!-- Speaker Id Field -->
<div class="form-group">
    {!! Form::label('speaker_id', 'Speaker Id:') !!}
    <p>{{ $schedule->speaker_id }}</p>
</div>

