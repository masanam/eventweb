<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::number('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_id', 'Event Id:') !!}
    {!! Form::number('event_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ticket Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ticket_id', 'Ticket Id:') !!}
    {!! Form::number('ticket_id', null, ['class' => 'form-control']) !!}
</div>

<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Private Reference Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('private_reference_number', 'Private Reference Number:') !!}
    {!! Form::text('private_reference_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Cancelled Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_cancelled', 'Is Cancelled:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_cancelled', 0) !!}
        {!! Form::checkbox('is_cancelled', '1', null) !!}
    </label>
</div>


<!-- Has Arrived Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_arrived', 'Has Arrived:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_arrived', 0) !!}
        {!! Form::checkbox('has_arrived', '1', null) !!}
    </label>
</div>


<!-- Arrival Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('arrival_time', 'Arrival Time:') !!}
    {!! Form::text('arrival_time', null, ['class' => 'form-control','id'=>'arrival_time']) !!}
</div>

@section('scripts')
   <script type="text/javascript">
           $('#arrival_time').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endsection

<!-- Account Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('account_id', 'Account Id:') !!}
    {!! Form::number('account_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Reference Index Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reference_index', 'Reference Index:') !!}
    {!! Form::number('reference_index', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Refunded Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_refunded', 'Is Refunded:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_refunded', 0) !!}
        {!! Form::checkbox('is_refunded', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.attendees.index') }}" class="btn btn-secondary">Cancel</a>
</div>
