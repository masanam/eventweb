<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{{ $attendee->order_id }}</p>
</div>

<!-- Event Id Field -->
<div class="form-group">
    {!! Form::label('event_id', 'Event Id:') !!}
    <p>{{ $attendee->event_id }}</p>
</div>

<!-- Ticket Id Field -->
<div class="form-group">
    {!! Form::label('ticket_id', 'Ticket Id:') !!}
    <p>{{ $attendee->ticket_id }}</p>
</div>

<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{{ $attendee->first_name }}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{{ $attendee->last_name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $attendee->email }}</p>
</div>

<!-- Private Reference Number Field -->
<div class="form-group">
    {!! Form::label('private_reference_number', 'Private Reference Number:') !!}
    <p>{{ $attendee->private_reference_number }}</p>
</div>

<!-- Is Cancelled Field -->
<div class="form-group">
    {!! Form::label('is_cancelled', 'Is Cancelled:') !!}
    <p>{{ $attendee->is_cancelled }}</p>
</div>

<!-- Has Arrived Field -->
<div class="form-group">
    {!! Form::label('has_arrived', 'Has Arrived:') !!}
    <p>{{ $attendee->has_arrived }}</p>
</div>

<!-- Arrival Time Field -->
<div class="form-group">
    {!! Form::label('arrival_time', 'Arrival Time:') !!}
    <p>{{ $attendee->arrival_time }}</p>
</div>

<!-- Account Id Field -->
<div class="form-group">
    {!! Form::label('account_id', 'Account Id:') !!}
    <p>{{ $attendee->account_id }}</p>
</div>

<!-- Reference Index Field -->
<div class="form-group">
    {!! Form::label('reference_index', 'Reference Index:') !!}
    <p>{{ $attendee->reference_index }}</p>
</div>

<!-- Is Refunded Field -->
<div class="form-group">
    {!! Form::label('is_refunded', 'Is Refunded:') !!}
    <p>{{ $attendee->is_refunded }}</p>
</div>

