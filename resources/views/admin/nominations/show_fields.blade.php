<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $nomination->title }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{{ $nomination->image }}</p>
</div>

<!-- Business Name Field -->
<div class="form-group">
    {!! Form::label('business_name', 'Business Name:') !!}
    <p>{{ $nomination->business_name }}</p>
</div>

<!-- Reason For Nomination Field -->
<div class="form-group">
    {!! Form::label('reason_for_nomination', 'Reason For Nomination:') !!}
    <p>{{ $nomination->reason_for_nomination }}</p>
</div>

<!-- No Of Nominations Field -->
<div class="form-group">
    {!! Form::label('no_of_nominations', 'No Of Nominations:') !!}
    <p>{{ $nomination->no_of_nominations }}</p>
</div>

<!-- Is Admin Selected Field -->
<div class="form-group">
    {!! Form::label('is_admin_selected', 'Is Admin Selected:') !!}
    <p>{{ $nomination->is_admin_selected }}</p>
</div>

<!-- Is Won Field -->
<div class="form-group">
    {!! Form::label('is_won', 'Is Won:') !!}
    <p>{{ $nomination->is_won }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $nomination->user_id }}</p>
</div>

