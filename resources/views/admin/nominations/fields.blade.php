<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::text('image', null, ['class' => 'form-control']) !!}
</div>

<!-- Business Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('business_name', 'Business Name:') !!}
    {!! Form::text('business_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Reason For Nomination Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reason_for_nomination', 'Reason For Nomination:') !!}
    {!! Form::text('reason_for_nomination', null, ['class' => 'form-control']) !!}
</div>

<!-- No Of Nominations Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_of_nominations', 'No Of Nominations:') !!}
    {!! Form::number('no_of_nominations', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Admin Selected Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_admin_selected', 'Is Admin Selected:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_admin_selected', 0) !!}
        {!! Form::checkbox('is_admin_selected', '1', null) !!}
    </label>
</div>


<!-- Is Won Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_won', 'Is Won:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_won', 0) !!}
        {!! Form::checkbox('is_won', '1', null) !!}
    </label>
</div>


<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.nominations.index') }}" class="btn btn-secondary">Cancel</a>
</div>
