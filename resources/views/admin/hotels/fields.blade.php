<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating', 'Rating:') !!}
    {!! Form::number('rating', null, ['class' => 'form-control']) !!}
</div>

<!-- Facility Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('facility', 'Facility:') !!}
    {!! Form::textarea('facility', null, ['class' => 'form-control']) !!}
</div>

<!-- Resto Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('resto', 'Resto:') !!}
    {!! Form::textarea('resto', null, ['class' => 'form-control']) !!}
</div>


<!-- Picture Field -->       
<div class="form-group col-sm-6">

{!! Form::label('image', 'Photo:') !!}
    <div id="picture-thumb">
        <img id="holder" src="{{ isset($hotel) ? $hotel->image : '' }}" style="width:500px">
    </div>
    <div class="input-group">

   <span class="input-group-btn">
     <a id="lfm" data-input="image" data-preview="holder" class="btn btn-primary text-white">
       <i class="fa fa-picture-o"></i> Choose
     </a>
   </span>
   <input class="form-control" type="text" id="image" name="image" value="{{ old('image', isset($hotel) ? $hotel->image : '') }}">
 </div>
 </div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.hotels.index') }}" class="btn btn-secondary">Cancel</a>
</div>
