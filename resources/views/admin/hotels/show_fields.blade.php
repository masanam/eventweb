<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $hotel->name }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $hotel->address }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $hotel->description }}</p>
</div>

<!-- Rating Field -->
<div class="form-group">
    {!! Form::label('rating', 'Rating:') !!}
    <p>{{ $hotel->rating }}</p>
</div>

<!-- Facility Field -->
<div class="form-group">
    {!! Form::label('facility', 'Facility:') !!}
    <p>{{ $hotel->facility }}</p>
</div>

<!-- Resto Field -->
<div class="form-group">
    {!! Form::label('resto', 'Resto:') !!}
    <p>{{ $hotel->resto }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{{ $hotel->image }}</p>
</div>

