<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $speaker->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $speaker->description }}</p>
</div>

<!-- Twitter Field -->
<div class="form-group">
    {!! Form::label('twitter', 'Twitter:') !!}
    <p>{{ $speaker->twitter }}</p>
</div>

<!-- Facebook Field -->
<div class="form-group">
    {!! Form::label('facebook', 'Facebook:') !!}
    <p>{{ $speaker->facebook }}</p>
</div>

<!-- Linkedin Field -->
<div class="form-group">
    {!! Form::label('linkedin', 'Linkedin:') !!}
    <p>{{ $speaker->linkedin }}</p>
</div>

<!-- Full Description Field -->
<div class="form-group">
    {!! Form::label('full_description', 'Full Description:') !!}
    <p>{{ $speaker->full_description }}</p>
</div>

