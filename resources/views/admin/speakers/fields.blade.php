<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Twitter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('twitter', 'Twitter:') !!}
    {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
</div>

<!-- Facebook Field -->
<div class="form-group col-sm-6">
    {!! Form::label('facebook', 'Facebook:') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<!-- Linkedin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('linkedin', 'Linkedin:') !!}
    {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_id', 'Event Id:') !!}
    {!! Form::number('event_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Picture Field -->       
<div class="form-group col-sm-6">

{!! Form::label('photo', 'Photo:') !!}
    <div id="picture-thumb">
        <img id="holder" src="{{ isset($speaker) ? $speaker->photo : '' }}" style="width:500px">
    </div>
    <div class="input-group">

   <span class="input-group-btn">
     <a id="lfm" data-input="photo" data-preview="holder" class="btn btn-primary text-white">
       <i class="fa fa-picture-o"></i> Choose
     </a>
   </span>
   <input class="form-control" type="text" id="photo" name="photo" value="{{ old('photo', isset($speaker) ? $speaker->photo : '') }}">
 </div>
 </div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.speakers.index') }}" class="btn btn-secondary">Cancel</a>
</div>
