<!-- Album Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('album_id', 'Album Id:') !!}
    {!! Form::number('album_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

<!-- Picture Field -->       
<div class="form-group col-sm-6">
{!! Form::label('picture', 'Picture:') !!}
    <div id="picture-thumb">
        <img id="holder" src="{{ isset($gallery) ? $gallery->picture : '' }}" style="width:100%">
    </div>
    <div class="input-group">

   <span class="input-group-btn">
     <a id="lfm" data-input="picture" data-preview="holder" class="btn btn-primary text-white">
       <i class="fa fa-picture-o"></i> Choose
     </a>
   </span>
   <input class="form-control" type="text" id="picture" name="picture" value="{{ old('picture', isset($gallery) ? $gallery->picture : '') }}">
 </div>
 </div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('links', 'Links:') !!}
    {!! Form::text('links', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.galleries.index') }}" class="btn btn-secondary">Cancel</a>
</div>
