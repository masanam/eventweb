<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $event->title }}</p>
</div>

<!-- Location Field -->
<div class="form-group">
    {!! Form::label('location', 'Location:') !!}
    <p>{{ $event->location }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $event->description }}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{{ $event->start_date }}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{{ $event->end_date }}</p>
</div>

<!-- Sale Date Field -->
<div class="form-group">
    {!! Form::label('sale_date', 'Sale Date:') !!}
    <p>{{ $event->sale_date }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $event->user_id }}</p>
</div>

<!-- Currency Id Field -->
<div class="form-group">
    {!! Form::label('currency_id', 'Currency Id:') !!}
    <p>{{ $event->currency_id }}</p>
</div>

<!-- Sales Volume Field -->
<div class="form-group">
    {!! Form::label('sales_volume', 'Sales Volume:') !!}
    <p>{{ $event->sales_volume }}</p>
</div>

<!-- Organiser Id Field -->
<div class="form-group">
    {!! Form::label('organiser_id', 'Organiser Id:') !!}
    <p>{{ $event->organiser_id }}</p>
</div>

<!-- Venue Name Field -->
<div class="form-group">
    {!! Form::label('venue_name', 'Venue Name:') !!}
    <p>{{ $event->venue_name }}</p>
</div>

<!-- Venue Name Full Field -->
<div class="form-group">
    {!! Form::label('venue_name_full', 'Venue Name Full:') !!}
    <p>{{ $event->venue_name_full }}</p>
</div>

<!-- Location Address Field -->
<div class="form-group">
    {!! Form::label('location_address', 'Location Address:') !!}
    <p>{{ $event->location_address }}</p>
</div>

<!-- Location Country Field -->
<div class="form-group">
    {!! Form::label('location_country', 'Location Country:') !!}
    <p>{{ $event->location_country }}</p>
</div>

<!-- Location State Field -->
<div class="form-group">
    {!! Form::label('location_state', 'Location State:') !!}
    <p>{{ $event->location_state }}</p>
</div>

<!-- Location Post Code Field -->
<div class="form-group">
    {!! Form::label('location_post_code', 'Location Post Code:') !!}
    <p>{{ $event->location_post_code }}</p>
</div>

<!-- Location Lat Field -->
<div class="form-group">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    <p>{{ $event->location_lat }}</p>
</div>

<!-- Location Long Field -->
<div class="form-group">
    {!! Form::label('location_long', 'Location Long:') !!}
    <p>{{ $event->location_long }}</p>
</div>

<!-- Location Google Place Id Field -->
<div class="form-group">
    {!! Form::label('location_google_place_id', 'Location Google Place Id:') !!}
    <p>{{ $event->location_google_place_id }}</p>
</div>

<!-- Pre Order Display Message Field -->
<div class="form-group">
    {!! Form::label('pre_order_display_message', 'Pre Order Display Message:') !!}
    <p>{{ $event->pre_order_display_message }}</p>
</div>

<!-- Post Order Display Message Field -->
<div class="form-group">
    {!! Form::label('post_order_display_message', 'Post Order Display Message:') !!}
    <p>{{ $event->post_order_display_message }}</p>
</div>

<!-- Social Share Text Field -->
<div class="form-group">
    {!! Form::label('social_share_text', 'Social Share Text:') !!}
    <p>{{ $event->social_share_text }}</p>
</div>

<!-- Social Show Facebook Field -->
<div class="form-group">
    {!! Form::label('social_show_facebook', 'Social Show Facebook:') !!}
    <p>{{ $event->social_show_facebook }}</p>
</div>

<!-- Social Show Linkedin Field -->
<div class="form-group">
    {!! Form::label('social_show_linkedin', 'Social Show Linkedin:') !!}
    <p>{{ $event->social_show_linkedin }}</p>
</div>

<!-- Social Show Twitter Field -->
<div class="form-group">
    {!! Form::label('social_show_twitter', 'Social Show Twitter:') !!}
    <p>{{ $event->social_show_twitter }}</p>
</div>

<!-- Social Show Email Field -->
<div class="form-group">
    {!! Form::label('social_show_email', 'Social Show Email:') !!}
    <p>{{ $event->social_show_email }}</p>
</div>

<!-- Social Show Googleplus Field -->
<div class="form-group">
    {!! Form::label('social_show_googleplus', 'Social Show Googleplus:') !!}
    <p>{{ $event->social_show_googleplus }}</p>
</div>

<!-- Location Is Manual Field -->
<div class="form-group">
    {!! Form::label('location_is_manual', 'Location Is Manual:') !!}
    <p>{{ $event->location_is_manual }}</p>
</div>

<!-- Is Live Field -->
<div class="form-group">
    {!! Form::label('is_live', 'Is Live:') !!}
    <p>{{ $event->is_live }}</p>
</div>

<!-- Barcode Type Field -->
<div class="form-group">
    {!! Form::label('barcode_type', 'Barcode Type:') !!}
    <p>{{ $event->barcode_type }}</p>
</div>

<!-- Event Image Field -->
<div class="form-group">
    {!! Form::label('event_image', 'Event Image:') !!}
    <p>{{ $event->event_image }}</p>
</div>

