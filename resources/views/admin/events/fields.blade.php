<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location', 'Location:') !!}
    {!! Form::text('location', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', 'Start Date:') !!}
    {!! Form::text('start_date', null, ['class' => 'form-control','id'=>'start_date']) !!}
</div>

@section('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           });

           $('#end_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           });
       </script>
@endsection

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', 'End Date:') !!}
    {!! Form::text('end_date', null, ['class' => 'form-control','id'=>'end_date']) !!}
</div>

<!-- Venue Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('venue_name', 'Venue Name:') !!}
    {!! Form::text('venue_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Picture Field -->       
<div class="form-group col-sm-6">
{!! Form::label('event_image', 'Event Image:') !!}
    <div id="picture-thumb">
        <img id="holder" src="{{ isset($event) ? $event->event_image : '' }}" style="width:100%">
    </div>
    <div class="input-group">

   <span class="input-group-btn">
     <a id="lfm" data-input="event_image" data-preview="holder" class="btn btn-primary text-white">
       <i class="fa fa-picture-o"></i> Choose
     </a>
   </span>
   <input class="form-control" type="text" id="event_image" name="event_image" value="{{ old('event_image', isset($event) ? $event->event_image : '') }}">
 </div>
 </div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.events.index') }}" class="btn btn-secondary">Cancel</a>
</div>
