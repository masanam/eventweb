<!DOCTYPE html>
<html class=" ">
    
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
        <title>Web App : Home Page</title>
        <meta content="Web App" name="description" />
        <meta content="themepassion" name="author" />

    <!-- App Icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/icons/favicon-16x16.png">
    <link rel="manifest" href="assets/images/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/images/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

        <link href="{{ asset('assets/css/preloader.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{ asset('assets/css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{ asset('assets/fonts/mdi/materialdesignicons.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{ asset('assets/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>

        <link href="{{ asset('assets/css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection" id="main-style"/>

        @yield('css')
        @laravelPWA
  </head>
    <!-- END HEAD -->

    <body class="  html"  data-header="light" data-footer="dark"  data-header_align="center"  data-menu_type="left" data-menu="light" data-menu_icons="on" data-footer_type="left" data-site_mode="light" data-footer_menu="show" data-footer_menu_style="light" >

<!-- START navigation -->
<nav class="fixedtop topbar navigation" role="navigation" style="width:360px;margin:0 auto;">
    <div class="">
      <a id="logo-container" href="/" class="brand-logo" style="margin-left:30px;">{{ $title }}</a>
      <a href="#" style="margin-left:10px;" data-target="slide-nav" class="waves-effect waves-circle navicon sidenav-trigger show-on-large"><i class="mdi mdi-menu" style="color:white;"></i></a>
    </div>
  </nav>

<ul id="slide-nav" class="sidenav sidemenu">
    <li class="menu-close"><i class="mdi mdi-close"></i></li>
    <li class="user-wrap">
      <div class="user-view row">
      <div class="col s3 imgarea">
        <a href="#user"><img class="circle" src="assets/images/user-37.jpg"></a></div>
      <div class="col s9 infoarea">
        <a href="#name"><span class="name">Administrator</span></a>
        <a href="#email"><span class="email">redbuzz@gmail.com</span></a>
      </div>
      </div>
    </li>
    
    <li class="menulinks">
      <ul class="collapsible">
      <!-- SIDEBAR - START -->

    <!-- MAIN MENU - START -->
            @foreach ($slides as $menu)
                    <li class="lvl1 ">
                    <div class=" waves-effect " >
                        <a href="{{ $menu['link'] }}">
                        <i class="{{ $menu['icon'] }}"></i>
                        <span class="title">{{ $menu['menu'] }}</span>
                            </a>
                        </div>
                    </li>
            @endforeach
                <!-- MAIN MENU - END -->
<!--  SIDEBAR - END -->
    </ul></li>
  </ul>
<!-- END navigation -->
  <div class="container">
    <div class="section">

    @yield('content')

      </div>
    </div>
  </div>

<footer class="page-footer">
    <div class="container footer-content">
      <div class="row">

        <div class="link-wrap">

            <ul class="social-wrap">
            <li class="social">
              <a class="" href="#!"><i class="mdi mdi-facebook"></i></a>
              <a class="" href="#!"><i class="mdi mdi-twitter"></i></a>
              <a class="" href="#!"><i class="mdi mdi-dribbble"></i></a>
              <a class="" href="#!"><i class="mdi mdi-google-plus"></i></a>
              <a class="" href="#!"><i class="mdi mdi-linkedin"></i></a>

            </li>
          </ul>
        </div>
      </div>
    </div>

  </footer>


  <div class="backtotop">
    <a class="btn-floating btn primary-bg">
      <i class="mdi mdi-chevron-up"></i>
    </a>
  </div>


<!-- PWA Service Worker Code -->

<script type="text/javascript">
  // This is the "Offline copy of pages" service worker
// Add this below content to your HTML page, or add the js file to your page at the very top to register service worker
// Check compatibility for the browser we're running this in
if ("serviceWorker" in navigator) {
  if (navigator.serviceWorker.controller) {
    console.log("[PWA Builder] active service worker found, no need to register");
  } else {
    // Register the service worker
    navigator.serviceWorker
      .register("pwabuilder-sw.js", {
        scope: "./"
      })
      .then(function (reg) {
        console.log("[PWA Builder] Service worker has been registered for scope: " + reg.scope);
      });
  }
}

</script>

<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->
<!-- CORE JS FRAMEWORK - START --> 
<script src="{{ asset('assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('assets/js/materialize.js') }}"></script>
<script src="{{ asset('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script> 
<!-- CORE JS FRAMEWORK - END --> 

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script type="text/javascript">
  $(document).ready(function(){
      
      $(".carousel-fullscreen.carousel-slider").carousel({
        fullWidth: true,
        indicators: true
      });
      setTimeout(autoplay, 3500);
      function autoplay() {
          $(".carousel").carousel("next");
          setTimeout(autoplay, 3500);
      }
         $(".slider3").slider({
                indicators: false,
                height: 200,
        });

  }); 
    </script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


<!-- CORE TEMPLATE JS - START --> 
<script src="{{ asset('assets/js/init.js') }}"></script>
<script src="{{ asset('assets/js/settings') }}.js"></script>
<script src="{{ asset('assets/js/scripts') }}.js"></script>
<!-- END CORE TEMPLATE JS - END --> 


<script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function(){
    $('.preloader-background').delay(10).fadeOut('slow');
  });
</script>

@yield('script')

</body>
</html>



