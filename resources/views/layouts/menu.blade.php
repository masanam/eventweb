<li class="nav-item">
              <a class="nav-link" href="/" target="_blank">
                <i class="nav-icon icon-speedometer"></i> Dashboard
                <span class="badge badge-primary">NEW</span>
              </a>
            </li>
            <li class="nav-title">Theme</li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route("admin.categories.index") }}">
                <i class="nav-icon icon-drop"></i> Categories</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route("admin.events.index") }}">
                <i class="nav-icon icon-pencil"></i> Events</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route("admin.speakers.index") }}">
                <i class="nav-icon icon-puzzle"></i> Speakers</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route("admin.schedules.index") }}">
                <i class="nav-icon icon-cursor"></i> Schedules</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route("admin.hotels.index") }}">
                <i class="nav-icon icon-pie-chart"></i> Hotels</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route("admin.tickets.index") }}">
                <i class="nav-icon icon-star"></i> Tickets</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route("admin.fAQS.index") }}">
                <i class="nav-icon icon-bell"></i> FAQ</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="{{ route("admin.galleries.index") }}">
                <i class="nav-icon icon-calculator"></i> Galleries</a>
            </li>

            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-cursor"></i> Surveys</a>
              <ul class="nav-dropdown-items">
              <li class="nav-item">
                <a class="nav-link" href="{{ route("admin.questions.index") }}">
                    <i class="nav-icon icon-calculator"></i> Questions</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route("admin.answers.index") }}">
                    <i class="nav-icon icon-calculator"></i> Response</a>
                </li>
                
              </ul>
            </li>

            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-bell"></i> Notifications</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="notifications/alerts.html">
                    <i class="nav-icon icon-bell"></i> Alerts</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="notifications/badge.html">
                    <i class="nav-icon icon-bell"></i> Badge</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="notifications/modals.html">
                    <i class="nav-icon icon-bell"></i> Modals</a>
                </li>
              </ul>
            </li>
            
            <li class="divider"></li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-star"></i> Pages</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="login.html" target="_top">
                    <i class="nav-icon icon-star"></i> Login</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="register.html" target="_top">
                    <i class="nav-icon icon-star"></i> Register</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="404.html" target="_top">
                    <i class="nav-icon icon-star"></i> Error 404</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="500.html" target="_top">
                    <i class="nav-icon icon-star"></i> Error 500</a>
                </li>
              </ul>
            </li>