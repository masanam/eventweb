@extends('layouts.pages_layout')

@section('content')

<div class="logo"><a href="#"><img src="images/header.png" style="width:100%"/></a></div>

<div class="row ui-mediabox circular" style="width:95%;margin:0 auto;">
    @foreach ($menus as $menu)
        <div class="col s4 text-center">
        <a class="img-wrap circle" href="{{ $menu['link'] }}">
            <img style="width: 100%;" src="{{ $menu['image'] }}">
            </a>
            {{ $menu['menu'] }}
        </div>
    @endforeach        
  </div>

  
<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 
  @endsection