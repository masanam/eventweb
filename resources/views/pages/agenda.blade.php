@extends('layouts.pages_layout')

@section('content')

    <!-- BEGIN BODY -->
  <div class="row search-page">
    <div class="col s12 pad-0">
      <ul class="tabs transparent z-depth-1">
        <li class="tab col s3"><a href="#test3-1">Day-1</a></li>
        <li class="tab col s3"><a href="#test4-1">Day-2</a></li>
      </ul>
    </div>
    
    <div id="test3-1" class="col s12 pad-0 ">
      <div class="tab-content z-depth-1">
        <h5 class="centersec-tit">{{ $events->start_date->format('d-F-Y') ?? '' }} </h5>
            <div class="timeline">
                <div class="verline"></div>
                @foreach($schedule1 as $key => $day1)
                @php ($color = ($key % 2 === 0) ? ' ' : 'accent-bg lighten-2 white-text')
                <div class="event">
                    <div class="date">
                        <h6>{{ substr($day1->start_time,0,5) ?? '' }}</h6>
                    </div>
                    <div class="card-panel {{ $color }} ">
                    <img src="{{ $day1->speaker->photo ?? '' }}" alt="{{ $day1->speaker->name ?? '' }}" title="{{ $day1->speaker->name ?? '' }}" class="circle">
                    <span style="font-size:80%;" class="photo">{{ $day1->speaker->name ?? '' }}</span>
                    <p class="title">{{ $day1->title ?? '' }}</p>
                    <p style="font-size:90%;">{{ $day1->subtitle ?? '' }}</p>
                  </div>

                </div>
                @endforeach

            </div>
        </div>
    </div>

    <div id="test4-1" class="col s12 pad-0 ">
    <div class="tab-content z-depth-1">
        <h5 class="centersec-tit">{{ $events->end_date->format('d-F-Y') ?? '' }} </h5>
            <div class="timeline">
                <div class="verline"></div>
                @foreach($schedule2 as $key => $day1)
                <div class="event">
                    <div class="date">
                        <h6>{{ substr($day1->start_time,0,5) ?? '' }}</h6>
                    </div>
                    <div class="card-panel {{ $color }} ">
                    <img src="{{ $day1->speaker->photo ?? '' }}" alt="{{ $day1->speaker->name ?? '' }}" title="{{ $day1->speaker->name ?? '' }}" class="circle">
                    <span style="font-size:80%;" class="photo">{{ $day1->speaker->name ?? '' }}</span>
                    <p class="title">{{ $day1->title ?? '' }}</p>
                    <p style="font-size:90%;">{{ $day1->subtitle ?? '' }}</p>
                  </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
  </div>
        


<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script type="text/javascript">
         $(".tabs").tabs();
         $("#tabs-swipe-demo").tabs({ swipeable: true });
    </script><script src="assets/plugins/fancybox/jquery.fancybox.min.js" type="text/javascript"></script>
<script type="text/javaScript">
  $("[data-fancybox=images]").fancybox({
  buttons : [ 
    "slideShow",
    "share",
    "zoom",
    "fullScreen",
    "close",
    "thumbs"
  ],
  thumbs : {
    autoStart : false
  }
});
 $("[data-fancybox=search-images]").fancybox({
  buttons : [ 
    "slideShow",
    "share",
    "zoom",
    "fullScreen",
    "close",
    "thumbs"
  ],
  thumbs : {
    autoStart : false
  }
});
</script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

@endsection
