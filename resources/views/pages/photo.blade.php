@extends('layouts.pages_layout')

@section('content')

    <div class="row ">
    <div class="col s12 pad-0"><h5 class="bot-20 sec-tit  ">Photos</h5>    
    <div class="row ui-mediabox ">

        @foreach($photos as $key => $photo)
        <div class="col s6">
        <a class="img-wrap" href="{{ $photo->picture ?? '' }}" data-fancybox="images" data-caption="{{ $photo->content ?? '' }}">
            <img class="z-depth-1"  style="width: 100%;" src="{{ $photo->picture ?? '' }}">
            </a>
        </div>
        @endforeach

        
    </div>    
    </div>
    </div>
    
    <div class="divider"></div>


<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="assets/js/masonry.pkgd.min.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(window).load(function(){
            $(".portfolio-masonry").masonry({
              itemSelector: ".col",
            });
        });
    </script><script src="assets/plugins/fancybox/jquery.fancybox.min.js" type="text/javascript"></script>
<script type="text/javaScript">
  $("[data-fancybox=images]").fancybox({
  buttons : [ 
    "slideShow",
    "share",
    "zoom",
    "fullScreen",
    "close",
    "thumbs"
  ],
  thumbs : {
    autoStart : false
  }
});
</script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
  @endsection