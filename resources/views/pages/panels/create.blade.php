@extends('layouts.pages_layout')

@section('css')
    <style>
        .title-field {
            position: relative;
            margin-top: 1rem;
            margin-bottom: 0rem;
        }
    </style>    
@endsection

@section('content')

<ul class="events">
    <form action="{{ url('panel/create') }}" method="post">
        @csrf
        <input type="hidden" name="account_id" value="1">
        <input type="hidden" name="user_id" value="1">
        <input type="hidden" name="organiser_id" value="1">

        <li class="event-item card">
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi mdi-pencil prefix"></i>
                    <input name="title" id="title" type="text" value="{{ old('title') }}" class="validate">
                    <label for="title">Title</label>
                </div>
                <div class="input-field col s12">
                    <i class="mdi mdi-keyboard-outline prefix"></i>
                    <textarea name="description" id="description" class="materialize-textarea validate">{{ old('description') }}</textarea>
                    <label for="description">Description</label>
                </div>
                <div class="input-field col s12">
                    <i class="mdi mdi-map-marker prefix"></i>
                    <input name="location" id="location" type="text" value="{{ old('location') }}" class="validate">
                    <label for="location">Location</label>
                </div>
                <div class="input-field col s12">
                    <i class="mdi mdi-home-map-marker prefix"></i>
                    <input name="venue_name" id="venue_name" type="text" value="{{ old('venue_name') }}" class="validate">
                    <label for="venue_name">Venue Name</label>
                </div>
                <div class="input-field col s12">
                    <i class="mdi mdi-ticket-account prefix"></i>
                    <input name="seat_kuota" id="seat_kuota" type="number" value="{{ old('seat_kuota') }}" class="validate">
                    <label for="seat_kuota">Seat Kuota</label>
                </div>
                <div class="title-field col s12">
                    <label for="start_date"><i class="mdi mdi-calendar prefix"></i> Date</label>
                </div>
                <div class="input-field col s6">
                    <input name="start_date" id="start_date" type="text" value="{{ old('start_date') }}"
                        class="datepicker validate">
                    <label for="start_date">Start</label>
                </div>
                <div class="input-field col s6">
                    <input name="end_date" id="end_date" type="text" value="{{ old('end_date') }}"
                        class="datepicker validate">
                    <label for="end_date">End</label>
                </div>
                <div class="title-field col s12">
                    <label for="start_date"><i class="mdi mdi-clock-outline prefix"></i> Time</label>
                </div>
                <div class="input-field col s6">
                    <input name="start_time" id="start_time" type="text" value="{{ old('start_time') }}"
                        class="timepicker validate">
                    <label for="start_time">Start</label>
                </div>
                <div class="input-field col s6">
                    <input name="end_time" id="end_time" type="text" value="{{ old('end_time') }}"
                        class="timepicker validate">
                    <label for="end_time">End</label>
                </div>
            </div>
            <div class="col s12 text-center">
                <input class="btn-large bg-primary" type="submit" value="Create">
            </div>
        </li>
    </form>
</ul>

@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('.timepicker').timepicker({
            twelveHour: false
        });
    });
</script>
@endsection