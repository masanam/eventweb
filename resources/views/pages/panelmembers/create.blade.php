@extends('layouts.pages_layout')

@section('content')

<ul class="events">
    <form action="{{ route('panel-member.store') }}" method="post">
        @csrf
        <input type="hidden" name="panel_id" value="{{ app('request')->input('panel') }}">
        <input type="hidden" name="account_id" value="1">

        <li class="event-item card">
        <div class="row">
          <div class="input-field col s12">
              <i class="mdi mdi-account prefix"></i>
              <input name="first_name" id="first_name" type="text" value="{{ old('first_name') }}" class="validate">
              <label for="first_name">First Name</label>
          </div>
          <div class="input-field col s12">
                <i class="mdi mdi-account prefix"></i>
              <input name="last_name" id="last_name" type="text" value="{{ old('last_name') }}" class="validate">
              <label for="last_name">Last Name</label>
          </div>
          <div class="input-field col s12">
                <i class="mdi mdi-email prefix"></i>
              <input name="email" id="email" type="email" value="{{ old('email') }}" class="validate">
              <label for="email">Email</label>
          </div>
          <div class="input-field col s12">
              <i class="mdi mdi-phone prefix"></i>
              <input name="mobile_number" id="mobile_number" type="number" value="{{ old('mobile_number') }}" class="validate">
              <label for="mobile_number">Phone Number</label>
          </div>
        </div>
        <div class="col s12 text-center">
            <input class="btn-large bg-primary" type="submit" value="Join">
          </div>
        </li>
   </form>
</ul>

@endsection