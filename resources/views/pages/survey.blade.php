@extends('layouts.pages_layout')

@section('content')

<ul class="events">
                            <form action="" method="post">
                                @csrf
                                @php ($session_id ='1')
                                <input type="hidden" name="session_id" value="{{ $session_id }}">
                                @foreach($questions as $question)
                                <li class="event-item card">
          <div class="col s12">
          <i class="mdi mdi-calendar-text prefix"></i>

                                 <label for="{{ $question->id }}">{{ $question->question }}</label>

                                        @if($question->type == 'free_text')
                                            <input name="answers[{{ $question->id }}]" id="{{ $question->id }}"
                                                   type="text">

                                        @elseif($question->type == 'numbers')
                                            <input name="answers[{{ $question->id }}]" id="{{ $question->id }}"
                                                   type="number">

                                        @elseif($question->type == 'single_answer')
                                            <select name="answers[{{ $question->id }}]" id="{{ $question->id }}"
                                                  >
                                                @foreach($question->options as $opt)
                                                    <option>{{ $opt }}</option>
                                                @endforeach
                                            </select>

                                        @elseif($question->type == 'multiple_answer')
                                            <select name="answers[{{ $question->id }}]" id="{{ $question->id }}" >
                                                @foreach($question->options as $opt)
                                                    <option>{{ $opt }}</option>
                                                @endforeach
                                            </select>

                                        @endif
                                        </div>
                                    </li>
                                @endforeach

                                <div class="col s12 text-center">
                                  <input class="btn-large bg-primary" type="submit" value="Send Survey">
                                </div>                         
   </form>
   </ul>

<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script type="text/javascript">
         $("input#input_text, textarea#textarea2").characterCounter();
    </script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script type="text/javascript">
         $("select").formSelect();
    </script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


  @endsection