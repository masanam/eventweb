@extends('layouts.pages_layout')

@section('content')
<ul class="collapsible expandable popout">
      @foreach($faqs as $key => $faq)
          <li class="{{ $loop->first ? ' active' : '' }}">
            <div class="collapsible-header">{{ $faq->question ?? '' }}</div>
            <div class="collapsible-body">
            <span>
            {{ $faq->answer ?? '' }}
            </span>
          </div>
          </li>
      @endforeach
  </ul>

<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script type="text/javascript">
if($(".collapsible").length){
        $(".collapsible").collapsible();
}
if($(".collapsible.expandable").length){
$(".collapsible.expandable").collapsible({
      accordion: false
    });
}    </script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
@endsection

