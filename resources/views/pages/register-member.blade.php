@extends('layouts.pages_layout')

@section('content')

<ul class="events">
    <form action="{{ url('register-member') }}" method="post">
        @csrf
        <input type="hidden" name="order_id" value="1">
        <input type="hidden" name="event_id" value="1">
        <input type="hidden" name="ticket_id" value="{{ app('request')->input('ticket') ?? 1 }}">
        <input type="hidden" name="account_id" value="1">
        <input type="hidden" name="reference_index" value="1">

        <li class="event-item card">
        <div class="row">
          <div class="input-field col s12">
              <i class="mdi mdi-account prefix"></i>
              <input name="first_name" id="first_name" type="text" value="{{ old('first_name') }}" class="validate">
              <label for="first_name">First Name</label>
          </div>
          <div class="input-field col s12">
                <i class="mdi mdi-account prefix"></i>
              <input name="last_name" id="last_name" type="text" value="{{ old('last_name') }}" class="validate">
              <label for="last_name">Last Name</label>
          </div>
          <div class="input-field col s12">
                <i class="mdi mdi-email prefix"></i>
              <input name="email" id="email" type="email" value="{{ old('email') }}" class="validate">
              <label for="email">Email</label>
          </div>
          <div class="input-field col s12">
              <i class="mdi mdi-phone prefix"></i>
              <input name="private_reference_number" id="private_reference_number" type="number" value="{{ old('private_reference_number') }}" class="validate">
              <label for="private_reference_number">Phone Number</label>
          </div>
        </div>
        <div class="col s12 text-center">
            <input class="btn-large bg-primary" type="submit" value="Register">
          </div>
        </li>
   </form>
</ul>

<!-- CORE JS FRAMEWORK - START --> 
<script src="{{ asset('assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('assets/js/materialize.js') }}"></script>
<script src="{{ asset('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script> 
<!-- CORE JS FRAMEWORK - END --> 


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script type="text/javascript">
         $("input#input_text, textarea#textarea2").characterCounter();
    </script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script type="text/javascript">
         $("select").formSelect();
    </script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


  @endsection