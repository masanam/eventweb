@extends('layouts.pages_layout')

@section('content')

<ul class="collection invoice-item" style="padding:10px;">

    
          <div class="chart-container">
          {{ PollWriter::draw(1) }}

          </div>
     

<div class="spacer-large">
</div>
</ul>


<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="assets/js/echarts.min.js" type="text/javascript"></script><script src="assets/js/chart-echarts.js" type="text/javascript"></script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

  @endsection