@extends('layouts.pages_layout')

@section('content')

<div class="bot-0">
  <ul id="tabs-swipe-demo" class="tabs tabs-swipable-ul z-depth-1">
  @php ($i=0)
    @foreach($tickets as $key => $ticket)
    @php ($i++)
    @php ($ticket_name = 'ticket-'.$i)
    <li class="tab col s3"><a href="#{{ $ticket_name }}">{{ $ticket->name }}</a></li>

    @endforeach
    
  </ul>
  </div>
  <div class="pricing-tabs">
    <div class="tabs-swipable">
    @php ($i=0)
    @foreach($tickets as $key => $ticket)
    @php ($i++)
    @php ($ticket_name = 'ticket-'.$i)
      <div id="{{ $ticket_name }}" class="col s12 z-depth-1">
      <div class="">
            <h2 class="center bg-primary"><sup>IDR</sup><span class="price">{{ $ticket->price }}</span>000</h2>
            <div class="price-row">
            {!! $ticket->description !!}
            </div>
            <div class="center">
            <a class="waves-effect waves-light bg-primary btn-large" href="{{ url('register-member?ticket='.$ticket->id) }}">Register</a>
            </div>
      </div>
  </div>
  @endforeach

</div>
</div>

<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script type="text/javascript">
         $("#tabs-swipe-demo").tabs({ swipeable: true });
    </script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

@endsection