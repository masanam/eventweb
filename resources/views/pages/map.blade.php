@extends('layouts.pages_layout')

@section('content')
<div id="test1-1" class="col s12 pad-0 ">
<div class="tab-content z-depth-1">

<div class="search-bar">
        <nav class="ui-forms">
            <div class="nav-wrapper">
              <form>
                <div class="input-field">
                  <input id="search" type="search" placeholder="Search Contacts" required>
                  <label for="search"><i class="mdi mdi-magnify"></i></label>
                  <i class="material-icons mdi mdi-close"></i>
                </div>
              </form>
            </div>
        </nav>
        </div>
        <div class="search-results contacts">
        
        <ul class="collection contacts z-depth-1">
            <li class="collection-item avatar">
              
              <a href="ui-app-chat.html" class='chatlink waves-effect'>                  <img src="assets/images/user-15.jpg" alt="Sherlyn Musich" title="Sherlyn Musich" class="circle">
                  <span class="title">Sherlyn Musich</span>
                  <p>We have never been to Asia, nor have we visited Africa.</p>
              </a>                <div class="secondary-content">
                    <a href="#!"><i class="mdi mdi-cellphone-iphone"></i></a>
                    <a href="#!"><i class="mdi mdi-message-outline"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <a href="ui-app-chat.html" class='chatlink waves-effect'>                  <img src="assets/images/user-17.jpg" alt="Lucie Hovey" title="Lucie Hovey" class="circle">
                  <span class="title">Lucie Hovey</span>
                  <p>Rock music approaches at high velocity.</p>
              </a>                <div class="secondary-content">
                    <a href="#!"><i class="mdi mdi-cellphone-iphone"></i></a>
                    <a href="#!"><i class="mdi mdi-message-outline"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <a href="ui-app-chat.html" class='chatlink waves-effect'>                  <img src="assets/images/user-31.jpg" alt="Sang Selander" title="Sang Selander" class="circle">
                  <span class="title">Sang Selander</span>
                  <p>Yes - I am halfway through it already!</p>
              </a>                <div class="secondary-content">
                    <a href="#!"><i class="mdi mdi-cellphone-iphone"></i></a>
                    <a href="#!"><i class="mdi mdi-message-outline"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <a href="ui-app-chat.html" class='chatlink waves-effect'>                  <img src="assets/images/user-20.jpg" alt="Georgine Vandine" title="Georgine Vandine" class="circle">
                  <span class="title">Georgine Vandine</span>
                  <p>The waves were crashing on the shore; it was a lovely sight.</p>
              </a>                <div class="secondary-content">
                    <a href="#!"><i class="mdi mdi-cellphone-iphone"></i></a>
                    <a href="#!"><i class="mdi mdi-message-outline"></i></a>
                </div>
            </li>


          </ul>        
        
        </div>
        </div>
        </div>

<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 
  @endsection