@extends('layouts.pages_layout')

@section('content')

@foreach($events as $key => $event)
<div class="carousel carousel-fullscreen carousel-slider about_carousel">
    <div class="carousel-item" href="#one!"><div class="bg" style="background-image: url('{{ $event->event_image ?? '' }}')"></div>
						<div class="item-content center-align white-text">
				              <div class="spacer-large"></div>
					          <h3>{{ $event->title ?? '' }}</h3>
        				</div>
    </div>
  </div>

  <div class="container">
    <div class="section">

     <!--   Icon Section   -->
        <div class="col s12 m12 l3">
            <h6>{{ $event->title ?? '' }}</h6>
            <p>{{ $event->description ?? '' }}</p>
        </div>

        <div class="col s12 m12 l3">
            <h6>When</h6>
            <p>{{ $event->start_date ?? '' }} to {{ $event->end_date ?? '' }}</p>
        </div>

        <div class="col s12 m12 l3">
            <h6>Where</h6>
            <p>{{ $event->venue_name ?? '' }}</p>
            <p>{{ $event->location ?? '' }}</p>
        </div>


    @endforeach

    <div class="divider"></div>
     <h5 class="center bot-20 sec-tit">Our Speaker</h5>

  <div class="row ui-mediabox team-box">
      @foreach($speakers as $key => $speaker)
        <div class="col s12 pad-0">
        <a class="img-wrap" href="{{ $speaker->photo ?? '' }}" data-fancybox="images" data-caption="{{ $speaker->name ?? '' }}">
        <img class="z-depth-1" style="width: 100%; max-height:250px;overflow:hidden;" src="{{ $speaker->photo ?? '' }}">
        </a>
        <h6 class="bot-0">{{ $speaker->name ?? '' }}</h6>
        <span class="text-upper light small">{{ $speaker->description ?? '' }}</span>
        <div class="spacer"></div>
        </div>
        @endforeach

    </div>

    </div>
  </div>

     <div class="row primary-bg">

<div class="divider"></div>
<h5 class="center bot-0 sec-tit white-text">Event Statistics</h5>
<p class="center-align white-text"></p>
        <div class="center col s12 pad-0">
            <i class="mdi mdi-trophy-outline" style="color:white;font-size: 36px;"></i>
            <h6 class="center white-text bot-0 light">Topics</h6>
            <h1 class="white-text center top-0">38</h1>
        </div>

        <div class="center col s12 pad-0">
        <i class="mdi mdi-book-open-page-variant" style="color:white;font-size: 36px;"></i>
            <h6 class="center white-text bot-0 light">Speakers</h6>
            <h1 class="white-text center top-0">40</h1>
        </div>
        <div class="center col s12 pad-0">
        <i class="mdi mdi-shape-outline" style="color:white;font-size: 36px;"></i>
            <h6 class="center white-text bot-0 light">Total Seats</h6>
            <h1 class="white-text center top-0">2350</h1>
        </div>
        <div class="center col s12 pad-0">
        <i class="mdi mdi-toolbox-outline" style="color:white;font-size: 36px;"></i>
            <h6 class="center white-text bot-0 light">Sponsors</h6>
            <h1 class="white-text center top-0">250</h1>
        </div>
</div>
  <div class="container">
    <div class="section">

     <h5 class="center sec-tit">About The Event</h5>
<p class="center-align">Mobile applications often stand in contrast to desktop applications which run on desktop computers, and with web applications which run in mobile web browsers rather than directly on the mobile device.</p>
<div class="center-align">
<a href="{{ url('register-member') }}" class="btn btn-rounded">Register</a>
</div>
<div class="spacer-large"></div>
  
    <div class="divider"></div>
    <div class="spacer"></div>

     <div class="spacer"></div>
<h5 class="center bot-20 sec-tit">People's Say</h5>


<div class="slider slider3 ">
    <ul class="slides transparent testimonials">
      <li>
		<p class="center"><i class="mdi mdi-format-quote-open"></i> We are so pleased with the purchase of this product. Zak has tons of components and features to deal with. You can really create anything you like.<i class="mdi mdi-format-quote-close"></i> </p>
		<div class=" center-align">
			<div class="row userinfo">
                    <div class="left-align">
                      <span class=""><strong>Phillip Eich</strong>
                        <br><span class='small'>CEO, Ink Ltd.</span>
                      </span>
                    </div>
        	</div>
        	</div>
      </li>

      <li>
		<p class="center"><i class="mdi mdi-format-quote-open"></i> We highly recommend using Zak for your next project. It is super quality and premium template that you can ask for. Just go for it.<i class="mdi mdi-format-quote-close"></i> </p>
		<div class=" center-align">
			<div class="row userinfo">
                    <div class="left-align">
                      <span class=""><strong>Arlean Howard</strong>
                        <br><span class='small'>Manager, Zed Ind.</span>
                      </span>
                    </div>
        	</div>
        	</div>
      </li>

      <li>
		<p class="center"><i class="mdi mdi-format-quote-open"></i> A perfect template to get you going for your next project. A full loaded feature packed template. It is multi purpose and super fast. Thank you for such a wonderful template.<i class="mdi mdi-format-quote-close"></i> </p>
		<div class=" center-align">
			<div class="row userinfo">
                    <div class="left-align">
                      <span class=""><strong>Rob Amaro</strong>
                        <br><span class='small'>Sr. Designer</span>
                      </span>
                    </div>
        	</div>
        	</div>
      </li>


    </ul>
  </div>
    </div>
  </div>

  
<footer class="page-footer">
    <div class="container footer-content">
      <div class="row">
        <div class="link-wrap">
          <ul class="link-ul">
            <li><a class="" href="#!"><i class="mdi mdi-phone"></i> +1 234 567 890</a></li>
            <li><a class="" href="#!"><i class="mdi mdi-email"></i> email@admin.com</a></li>
            <li><a class="" href="#!"><i class="mdi mdi-map-marker"></i> Jakarta-Indonesia</a></li>
            </ul>

        </div>
      </div>
    </div>
  </footer>

<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script type="text/javascript">
  $(document).ready(function(){
      
      $(".carousel-fullscreen.carousel-slider").carousel({
        fullWidth: true,
        indicators: true
      });
      setTimeout(autoplay, 3500);
      function autoplay() {
          $(".carousel").carousel("next");
          setTimeout(autoplay, 3500);
      }

      $(".slider3").slider({
                indicators: false,
                height: 200,
     });

  }); 
    </script><script src="assets/plugins/fancybox/jquery.fancybox.min.js" type="text/javascript"></script>
<script type="text/javaScript">
  $("[data-fancybox=images]").fancybox({
  buttons : [ 
    "slideShow",
    "share",
    "zoom",
    "fullScreen",
    "close",
    "thumbs"
  ],
  thumbs : {
    autoStart : false
  }
});
</script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

  @endsection