@extends('layouts.pages_layout')

@section('content')
<ul class="collection notifications z-depth-1">
            <li class="collection-item avatar">
              <div class="notify" >
                  <img src="assets/images/user-18.jpg" alt="Dion Vitale" title="Dion Vitale" class="circle">
                  
                  <p>The sky is clear; the stars are twinkling.</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Reply</span></a>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Send</span></a>

                  <span class="time">5 mins</span>
              </div>
                <div class="secondary-content">
                   <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify" >
                  <img src="assets/images/user-39.jpg" alt="Garret Ferreri" title="Garret Ferreri" class="circle">
                  
                  <p>She works two jobs to make ends meet</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Message</span></a>
                  <span class="time">10 mins</span>
              </div>
                <div class="secondary-content">
                   <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify">
                  <img src="assets/images/user-2.jpg" alt="Ezra Coghill" title="Ezra Coghill" class="circle">
                  
                  <p>Understanding other people perspective and concepts</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Take Action</span></a>
                  <span class="time">15 mins</span>
              </div>
                <div class="secondary-content">
                    <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify" >
                  <img src="assets/images/user-17.jpg" alt="Lucie Hovey" title="Lucie Hovey" class="circle">
                  
                  <p>Good question - I am still trying to figure that out!</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Reply</span></a>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Send</span></a>

                  <span class="time">45 mins</span>
              </div>
                <div class="secondary-content">
                   <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify" >
                  <img src="assets/images/user-10.jpg" alt="Randell Sinkler" title="Randell Sinkler" class="circle">
                  
                  <p>She borrowed the book from him many years ago</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Message</span></a>
                  <span class="time">1 hour</span>
              </div>
                <div class="secondary-content">
                   <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify">
                  <img src="assets/images/user-27.jpg" alt="Phillip Eich" title="Phillip Eich" class="circle">
                  
                  <p>I want more detailed information.</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Take Action</span></a>
                  <span class="time">5 hours</span>
              </div>
                <div class="secondary-content">
                    <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify" >
                  <img src="assets/images/user-41.jpg" alt="Kyle Stillson" title="Kyle Stillson" class="circle">
                  
                  <p>My life is a crazy explosion of shapes and colors</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Reply</span></a>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Send</span></a>

                  <span class="time">Yesterday</span>
              </div>
                <div class="secondary-content">
                   <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify" >
                  <img src="assets/images/user-3.jpg" alt="Rocky Coots" title="Rocky Coots" class="circle">
                  
                  <p>Yes, but I only have a couple of items on it</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Message</span></a>
                  <span class="time">14th Mar</span>
              </div>
                <div class="secondary-content">
                   <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify">
                  <img src="assets/images/user-15.jpg" alt="Sherlyn Musich" title="Sherlyn Musich" class="circle">
                  
                  <p>Everyone was busy, so I went to the movie alone.</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Take Action</span></a>
                  <span class="time">16th Mar</span>
              </div>
                <div class="secondary-content">
                    <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify" >
                  <img src="assets/images/user-26.jpg" alt="Dino Santana" title="Dino Santana" class="circle">
                  
                  <p>She was too short to see over the fence.</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Reply</span></a>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Send</span></a>

                  <span class="time">16th Mar</span>
              </div>
                <div class="secondary-content">
                   <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify" >
                  <img src="assets/images/user-34.jpg" alt="Karole Mcclaine" title="Karole Mcclaine" class="circle">
                  
                  <p>Rock music approaches at high velocity.</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Message</span></a>
                  <span class="time">17th Mar</span>
              </div>
                <div class="secondary-content">
                   <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify">
                  <img src="assets/images/user-8.jpg" alt="Denny Veiga" title="Denny Veiga" class="circle">
                  
                  <p>The intellectual activity that produces material</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Take Action</span></a>
                  <span class="time">17th Mar</span>
              </div>
                <div class="secondary-content">
                    <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify" >
                  <img src="assets/images/user-4.jpg" alt="Bob Curl" title="Bob Curl" class="circle">
                  
                  <p>Let me help you with your baggage.</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Reply</span></a>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Send</span></a>

                  <span class="time">18th Mar</span>
              </div>
                <div class="secondary-content">
                   <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify" >
                  <img src="assets/images/user-31.jpg" alt="Sang Selander" title="Sang Selander" class="circle">
                  
                  <p>Full time manager and rockin' it!</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Message</span></a>
                  <span class="time">19th Mar</span>
              </div>
                <div class="secondary-content">
                   <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            <li class="collection-item avatar">
              <div class="notify">
                  <img src="assets/images/user-16.jpg" alt="Millard Smith" title="Millard Smith" class="circle">
                  
                  <p>My friends encouraged me to do so, so i persue</p>
                  <a href="#!"><span class="new badge bg-primary" data-badge-caption="">Take Action</span></a>
                  <span class="time">20th Mar</span>
              </div>
                <div class="secondary-content">
                    <a href="#!" class="waves-effect"><i class="mdi mdi-delete-sweep"></i></a>
                </div>
            </li>
            
          </ul>

<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 

  @endsection