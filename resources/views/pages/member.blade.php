@extends('layouts.pages_layout')

@section('css')
<style>
    .fixed-action-btn {
        bottom: 60px;
        left: 20px;
        width: 55px;
    }

    .btn-join {
        position: absolute;
        top: 4px;
        right: 4px;
        font-size: 12px;
        text-transform: none;
    }
</style>
@endsection

@section('content')

<ul class="events">

    @forelse($panels as $panel)
    <li class="event-item card">
        <div class="title">{{ $panel->title }}</div>
        <div class="time">
            {{ date('d M', strtotime($panel->start_date)).date('-d M Y', strtotime($panel->end_date))}},
            {{ date('h:ia', strtotime($panel->start_time))}}-{{ date('h:ia', strtotime($panel->end_time))}}
        </div>
        <div class="people">
            @forelse($panel->panelMember->slice(0, 5) as $member)
            <a href="#"><img src="assets/images/user-27.jpg" alt="{{ $member->first_name.' '.$member->last_name }}" title="{{ $member->first_name.' '.$member->last_name }}" class="circle"></a>
            @empty
            {{-- if null --}}
            @endforelse
        </div>
        <a href="{{ url('panel-member?panel='.$panel->id) }}" class="btn waves-effect waves-light btn-join">
            Join
        </a>
        <div class="actions">
            <a href="#" class='waves-effect'> <i class="mdi mdi-square-edit-outline"></i></a>
            <a href="#"><i class="mdi mdi-delete"></i></a>
        </div>
    </li>
    @empty
    <p>Data not found!</p>
    @endforelse
</ul>

<div class="fixed-action-btn">
    <a href="{{ route('panel.create') }}" class="btn-floating btn-large waves-effect waves-light primary"><i class="mdi mdi-plus">add</i></a>
</div>

@endsection