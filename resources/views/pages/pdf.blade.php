@extends('layouts.pages_layout')

@section('content')

    <div class="row ui-mediabox blogs ">
        @forelse($pdfs as $pdf)
        <div class="col s12">
                <div class="blog-img-wrap">
                    <a class="img-wrap" href="{{ $pdf->links }}" data-fancybox="images" data-caption="{{ $pdf->title }}">
                    <img class="z-depth-1" style="" src="{{ $pdf->picture }}">
                    </a>
                </div>
                <div class="blog-info">              
                    <h5 class="title">{{ $pdf->title }}</h5>
                    <span class="small date">{{ date('d M Y, D' , strtotime($pdf->created_at)) }}</span>
                    <span class="small tags">
                        <a class="small" href="#!">Web Design</a>, <a class="small" href="#!">Graphics</a>
                    </span>
                    <p class="bot-0 text">{{ $pdf->content }}</p>
                    <br>
                    <a class="waves-effect waves-light btn " href="{{ $pdf->links }}">Download PDF</a>
            </div>
        </div>
        @empty
            <p>Data not found!</p>
        @endforelse
    </div>


<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END -->
  @endsection