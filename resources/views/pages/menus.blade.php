@extends('layouts.pages_layout')

@section('content')


<div class="bot-0">
  <ul id="tabs-swipe-demo" class="tabs tabs-swipable-ul z-depth-1">

    @php ($i=0)
    @foreach($hotels as $key => $hotel)
    @php ($i++)
    @php ($hotel_name = 'hotel-'.$i)
    <li class="tab col s3"><a href="#{{ $hotel_name }}">{{ $hotel_name }}</a></li>

    @endforeach
  </ul>
  </div>
  <div class="pricing-tabs">
    <div class="tabs-swipable">
    @php ($i=0)
    @foreach($hotels as $key => $hotel)
    @php ($i++)
    @php ($hotel_name = 'hotel-'.$i)
        <div id="{{ $hotel_name }}" class="col s12 z-depth-1">
                    <h2 class="center" style="background-image: url('{{ $hotel->image ?? '' }}'); background-size:100% 100%;">
                        {{ $hotel->name }}
                    </h2>
                    <div class="price-row">
                        <div>{{ $hotel->description }}</div>
                        <div>{!! $hotel->facility !!}</div>
                        <div>{!! $hotel->resto !!}</div>

                    </div>
                    <div class="center">
                    </div>
        </div>
  @endforeach

</div>
</div>

<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script type="text/javascript">
         $("#tabs-swipe-demo").tabs({ swipeable: true });
    </script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

@endsection