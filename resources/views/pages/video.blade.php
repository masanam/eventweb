@extends('layouts.pages_layout')

@section('content')



<div class="row ui-mediabox blogs ">
@foreach($videos as $key => $video)
        <div class="col s12">
                <div class="blog-img-wrap">
                @php ( $youtube = str_replace('watch?v=','embed/',$video->links))
                <iframe src="{{ $youtube }}" width="350" height="200" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="blog-info">
                <a href="ui-pages-blog-item.html" >                    
                <h5 class="title">{{ $video->title }}</h5>
                </a>                
                <span class="small date">{{ $video->created_at->format('d F Y') }}</span>
                <p class="bot-0 text">{{ $video->content }}</p>
                <div class="spacer"></div>
                <div class="divider"></div>
                <div class="spacer"></div>
            </div>
        </div>
@endforeach


    </div>

<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 
  @endsection