@extends('layouts.pages_layout')

@section('content')
<div id="test1-1" class="col s12 pad-0 ">
<div class="tab-content z-depth-1">

<div class="search-bar">
        <nav class="ui-forms">
            <div class="nav-wrapper">
              <form>
                <div class="input-field">
                  <input id="search" type="search" placeholder="Search Contacts" required>
                  <label for="search"><i class="mdi mdi-magnify"></i></label>
                  <i class="material-icons mdi mdi-close"></i>
                </div>
              </form>
            </div>
        </nav>
        </div>
        <div class="search-results contacts">
        
        <ul class="collection contacts z-depth-1">
        @foreach($speakers as $key => $speaker)

            <li class="collection-item avatar">
              
              <a href="ui-app-chat.html" class='chatlink waves-effect'>                 
                 <img src="{{ $speaker->photo ?? '' }}" alt="{{ $speaker->name ?? '' }}" title="{{ $speaker->name ?? '' }}" class="circle">
                  <span class="title">{{ $speaker->name ?? '' }}</span>
                  <p>{{ $speaker->description ?? '' }}</p>
              </a>                <div class="secondary-content">
                    <a href="#!"><i class="mdi mdi-cellphone-iphone"></i></a>
                    <a href="#!"><i class="mdi mdi-message-outline"></i></a>
                </div>
            </li>
            @endforeach


          </ul>        
        
        </div>
        </div>
        </div>

<!-- CORE JS FRAMEWORK - START --> 
<script src="assets/js/jquery-2.2.4.min.js"></script>
<script src="assets/js/materialize.js"></script>
<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script> 
<!-- CORE JS FRAMEWORK - END --> 
  @endsection