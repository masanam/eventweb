<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('speakers', 'Admin\SpeakerAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('schedules', 'Admin\ScheduleAPIController');
});




Route::group(['prefix' => 'admin'], function () {
    Route::resource('events', 'Admin\EventAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('f_a_q_s', 'Admin\FAQAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('attendees', 'Admin\AttendeeAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('categories', 'Admin\CategoryAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('questions', 'Admin\QuestionAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('answers', 'Admin\AnswerAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('nominations', 'Admin\NominationAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('votings', 'Admin\VotingAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('hotel--from_tables', 'Admin\Hotel--fromTableAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('hotels', 'Admin\HotelAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('albums', 'Admin\AlbumAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('galleries', 'Admin\GalleryAPIController');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('tickets', 'Admin\TicketAPIController');
});
