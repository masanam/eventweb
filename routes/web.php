<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'PagesController@viewLanding');

Route::get('/socialfeed', 'PagesController@viewSocial');
Route::get('/eventinfo', 'PagesController@viewEvent');
Route::get('/hotel', 'PagesController@viewMenu');
Route::get('/agenda', 'PagesController@viewAgenda');
Route::get('/speaker', 'PagesController@viewSpeaker');
Route::get('/member', 'PagesController@viewMember');
Route::get('/faq', 'PagesController@viewFaq');
Route::get('/polling', 'PagesController@viewPolling');
Route::get('/photo', 'PagesController@viewPhoto');
Route::get('/video', 'PagesController@viewVideo');
Route::get('/survey', 'PagesController@viewSurvey');
Route::get('/pdf', 'PagesController@viewPdf');
Route::get('/map', 'PagesController@viewMap');
Route::get('/ticket', 'PagesController@viewTicket');
Route::get('/sponsor', 'PagesController@viewSponsor');

// Register Member
Route::get('/register-member', 'MemberController@create');
Route::post('/register-member', 'MemberController@store');

// Panel
Route::get('/panel/create', 'PanelController@create')->name('panel.create');
Route::post('/panel/create', 'PanelController@store')->name('panel.store');;

// Panel Member
Route::get('/panel-member', 'PanelMemberController@create')->name('panel-member.create');
Route::post('/panel-member', 'PanelMemberController@store')->name('panel-member.store');;

Route::post('/vote/polls/{poll}', 'VoteManagerController@vote')->name('poll.vote');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');

Route::group(['prefix' => 'admin'], function () {
    Route::resource('speakers', 'Admin\SpeakerController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('schedules', 'Admin\ScheduleController', ["as" => 'admin']);
});



Route::group(['prefix' => 'admin'], function () {
    Route::resource('events', 'Admin\EventController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('fAQS', 'Admin\FAQController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('attendees', 'Admin\AttendeeController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('categories', 'Admin\CategoryController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('questions', 'Admin\QuestionController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('answers', 'Admin\AnswerController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('nominations', 'Admin\NominationController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('votings', 'Admin\VotingController', ["as" => 'admin']);
});



Route::group(['prefix' => 'admin'], function () {
    Route::resource('hotels', 'Admin\HotelController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('albums', 'Admin\AlbumController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('galleries', 'Admin\GalleryController', ["as" => 'admin']);
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('tickets', 'Admin\TicketController', ["as" => 'admin']);
});

    Route::get('/admin', ['uses' => 'PollManagerController@home', 'as' => 'poll.home']);
    Route::get('/admin/polls', ['uses' => 'PollManagerController@index', 'as' => 'poll.index']);
    Route::get('/admin/polls/create', ['uses' => 'PollManagerController@create', 'as' => 'poll.create']);
    Route::get('/admin/polls/{poll}', ['uses' => 'PollManagerController@edit', 'as' => 'poll.edit']);
    Route::patch('/admin/polls/{poll}', ['uses' => 'PollManagerController@update', 'as' => 'poll.update']);
    Route::delete('/admin/polls/{poll}', ['uses' => 'PollManagerController@remove', 'as' => 'poll.remove']);
    Route::patch('/admin/polls/{poll}/lock', ['uses' => 'PollManagerController@lock', 'as' => 'poll.lock']);
    Route::patch('/admin/polls/{poll}/unlock', ['uses' => 'PollManagerController@unlock', 'as' => 'poll.unlock']);
    Route::post('/admin/polls', ['uses' => 'PollManagerController@store', 'as' => 'poll.store']);
    Route::get('/admin/polls/{poll}/options/add', ['uses' => 'OptionManagerController@push', 'as' => 'poll.options.push']);
    Route::post('/admin/polls/{poll}/options/add', ['uses' => 'OptionManagerController@add', 'as' => 'poll.options.add']);
    Route::get('/admin/polls/{poll}/options/remove', ['uses' => 'OptionManagerController@delete', 'as' => 'poll.options.remove']);
    Route::delete('/admin/polls/{poll}/options/remove', ['uses' => 'OptionManagerController@remove', 'as' => 'poll.options.remove']);
