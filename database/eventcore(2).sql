-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 05, 2020 at 03:15 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventcore`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seotitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `created_by` int(11) NOT NULL DEFAULT '1',
  `updated_by` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `title`, `seotitle`, `active`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Photos', NULL, 'Y', 1, 1, NULL, NULL, NULL),
(2, 'Videos', NULL, 'Y', 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `amenities`
--

DROP TABLE IF EXISTS `amenities`;
CREATE TABLE IF NOT EXISTS `amenities` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `amenity_price`
--

DROP TABLE IF EXISTS `amenity_price`;
CREATE TABLE IF NOT EXISTS `amenity_price` (
  `price_id` int(10) UNSIGNED NOT NULL,
  `amenity_id` int(10) UNSIGNED NOT NULL,
  KEY `price_id_fk_384063` (`price_id`),
  KEY `amenity_id_fk_384063` (`amenity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendees`
--

DROP TABLE IF EXISTS `attendees`;
CREATE TABLE IF NOT EXISTS `attendees` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `ticket_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `private_reference_number` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_cancelled` tinyint(1) NOT NULL DEFAULT '0',
  `has_arrived` tinyint(1) NOT NULL DEFAULT '0',
  `arrival_time` datetime DEFAULT NULL,
  `account_id` int(10) UNSIGNED NOT NULL,
  `reference_index` int(11) NOT NULL DEFAULT '0',
  `is_refunded` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `attendees_event_id_index` (`event_id`),
  KEY `attendees_account_id_index` (`account_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendees`
--

INSERT INTO `attendees` (`id`, `order_id`, `event_id`, `ticket_id`, `first_name`, `last_name`, `email`, `private_reference_number`, `created_at`, `updated_at`, `deleted_at`, `is_cancelled`, `has_arrived`, `arrival_time`, `account_id`, `reference_index`, `is_refunded`) VALUES
(1, 1, 1, 1, 'AAA', 'BBB', 'aaa@gmail.com', '122', '2020-02-13 01:03:33', '2020-02-13 01:03:33', NULL, 0, 0, '2020-02-13 10:00:00', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Technology', '2020-02-12 23:31:54', '2020-02-12 23:31:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `sale_date` datetime DEFAULT NULL,
  `account_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `currency_id` int(10) UNSIGNED DEFAULT NULL,
  `sales_volume` decimal(13,2) NOT NULL DEFAULT '0.00',
  `organiser_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `venue_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `venue_name_full` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_address` varchar(355) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_post_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lat` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_long` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_google_place_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pre_order_display_message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `post_order_display_message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `social_share_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `social_show_facebook` tinyint(1) NOT NULL DEFAULT '1',
  `social_show_linkedin` tinyint(1) NOT NULL DEFAULT '1',
  `social_show_twitter` tinyint(1) NOT NULL DEFAULT '1',
  `social_show_email` tinyint(1) NOT NULL DEFAULT '1',
  `social_show_googleplus` tinyint(1) NOT NULL DEFAULT '1',
  `location_is_manual` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_live` tinyint(1) NOT NULL DEFAULT '0',
  `barcode_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'QRCODE',
  `event_image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `location`, `description`, `start_date`, `end_date`, `sale_date`, `account_id`, `user_id`, `currency_id`, `sales_volume`, `organiser_id`, `venue_name`, `venue_name_full`, `location_address`, `location_country`, `location_state`, `location_post_code`, `location_lat`, `location_long`, `location_google_place_id`, `pre_order_display_message`, `post_order_display_message`, `social_share_text`, `social_show_facebook`, `social_show_linkedin`, `social_show_twitter`, `social_show_email`, `social_show_googleplus`, `location_is_manual`, `is_live`, `barcode_type`, `event_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Most Investable Startups', 'Jakarta', 'Next Indonesian Unicorns (NextICorn) platform streamlines and promotes Indonesia’s most investable startups to the most capable investors across the globe.', '2020-02-13 00:00:00', '2020-02-14 00:00:00', '2020-02-13 00:00:00', 1, 1, 1, '1.00', 1, 'Ballroom', 'Bali Ballrom', 'Jl. Nusa DUa', 'Indonesia', 'Bali', '15224', '1000', '1000', '1', 'AFTECH Professionals Gathering Series 1', 'AFTECH Professionals Gathering Series 1', 'AFTECH Professionals Gathering Series 1', 0, 0, 0, 0, 0, 1, 0, '1', 'http://localhost:8000/storage/files/1/Event/fintech.jpg', '2020-02-13 00:03:54', '2020-02-13 00:03:54', NULL),
(2, 'Evening Presentation', 'Bali', 'Evening Presentation', '2020-02-29 10:00:00', '2020-02-24 17:00:00', NULL, 1, 1, NULL, '0.00', 1, 'Hotel Nusa Dua', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 0, 0, 'QRCODE', 'http://localhost:8000/storage/files/1/Event/AFTECH-BRI-MOU.jpg', '2020-02-14 03:41:47', '2020-02-14 03:41:47', '2020-02-14 03:41:47');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'What are the type of subscription plans offered?', 'The verb to design expresses the process of developing a design. In some cases, the direct construction of an object without an explicit prior plan (such as in craftwork, some engineering, coding, & graphic design) may also be a design activity.', '2020-02-13 01:01:57', '2020-02-14 03:58:42', NULL),
(2, 'How to setup new account?', 'A design is a plan or specification for the construction of an object or system or for the implementation of an activity or process, or the result of that plan or specification in the form of a prototype, product or process.', '2020-02-14 03:58:02', '2020-02-14 03:58:02', NULL),
(3, 'What are the renewal policies available?', 'It may involve considerable research, thought, modeling, interactive adjustment, and re-design. Meanwhile, diverse kinds of objects may be designed, including clothing, graphical user interfaces, products, skyscrapers, corporate identities, business processes, and even methods or processes of designing.', '2020-02-14 03:59:08', '2020-02-14 03:59:08', NULL),
(4, 'What are the privacy terms of your company?', 'Another definition of design is planning to manufacture an object, system, component or structure. Thus the word \'design\' can be used as a noun or a verb. In a broader sense, design is an applied art and engineering that integrates with technology.', '2020-02-14 03:59:33', '2020-02-14 03:59:33', NULL),
(5, 'Customer services and support policies of our company', 'She looked at her student wondering if she could ever get through. \'You need to learn to think for yourself,\' she wanted to tell him. \'Your friends are holding you back and bringing you down.\' But she didn\'t because she knew his friends were all that he had and even if that meant a life of misery, he would never give them up.', '2020-02-14 03:59:54', '2020-02-14 03:59:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `album_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `links` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT '1',
  `updated_by` int(11) DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `album_id`, `title`, `content`, `picture`, `links`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(9, 1, 'Album 1', 'test', 'http://localhost:8000/storage/files/1/photo/blog-101.jpg', NULL, NULL, NULL, NULL, '2020-02-20 00:12:21', '2020-02-20 00:12:21'),
(10, 1, 'Album 2', NULL, 'http://localhost:8000/storage/files/1/photo/blog-102.jpg', NULL, 1, 1, NULL, '2020-02-20 00:15:01', '2020-02-20 00:15:01'),
(11, 1, 'Album 1', NULL, 'http://localhost:8000/storage/files/1/photo/blog-103.jpg', NULL, 1, 1, NULL, '2020-02-20 00:15:48', '2020-02-20 00:15:48'),
(12, 1, 'Album 1', NULL, 'http://localhost:8000/storage/files/1/photo/blog-104.jpg', NULL, 1, 1, NULL, '2020-02-20 00:16:09', '2020-02-20 00:16:09'),
(13, 1, 'Album 1', NULL, 'http://localhost:8000/storage/files/1/photo/blog-105.jpg', NULL, 1, 1, NULL, '2020-02-20 00:16:32', '2020-02-20 00:16:32'),
(14, 2, 'Video 1', NULL, NULL, 'https://www.youtube.com/watch?v=5np6xefSMkE', 1, 1, NULL, '2020-02-20 00:34:35', '2020-02-20 00:57:01'),
(15, 2, 'Figma', NULL, NULL, 'https://www.youtube.com/watch?v=Vo0sEPqArRQ', 1, 1, NULL, '2020-02-20 01:02:06', '2020-02-20 01:02:06');

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

DROP TABLE IF EXISTS `hotels`;
CREATE TABLE IF NOT EXISTS `hotels` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `rating` int(11) DEFAULT NULL,
  `facility` text COLLATE utf8mb4_unicode_ci,
  `resto` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `name`, `address`, `description`, `rating`, `facility`, `resto`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'IBIS HOTEL', NULL, '0.4 Mile from the Venue', 5, 'Extra bed <br/>\r\nInternet <br/>\r\nBar <br/>\r\nMusic Lounge <br/>', NULL, 'http://localhost:8000/storage/files/1/hotel/3.jpg', '2020-02-06 03:09:54', '2020-02-17 01:13:08', NULL),
(2, 'SANUR BEACH', NULL, '0.5 Mile from the Venue', 4, 'Extra bed <br/>\r\nInternet <br/>\r\nBar <br/>\r\nMusic Lounge <br/>', NULL, 'http://localhost:8000/storage/files/1/hotel/2.jpg', '2020-02-06 03:09:55', '2020-02-17 01:12:55', NULL),
(3, 'WHIZZ HOTEL', NULL, '0.6 Mile from the Venue', 3, 'Extra bed <br/>\r\nInternet <br/>\r\nBar <br/>\r\nMusic Lounge <br/>', 'Extra bed <br/>\r\nInternet <br/>\r\nBar <br/>\r\nMusic Lounge <br/>', 'http://localhost:8000/storage/files/1/hotel/1.jpg', '2020-02-06 03:09:55', '2020-02-17 01:12:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `larapoll_options`
--

DROP TABLE IF EXISTS `larapoll_options`;
CREATE TABLE IF NOT EXISTS `larapoll_options` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poll_id` int(10) UNSIGNED NOT NULL,
  `votes` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `larapoll_options_poll_id_foreign` (`poll_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `larapoll_options`
--

INSERT INTO `larapoll_options` (`id`, `name`, `poll_id`, `votes`, `created_at`, `updated_at`) VALUES
(1, 'Aplikasi', 1, 0, '2020-02-25 03:39:10', '2020-02-25 03:39:10'),
(2, 'Website', 1, 0, '2020-02-25 03:39:10', '2020-02-25 03:39:10'),
(3, 'OnlineShop', 1, 0, '2020-02-25 03:39:10', '2020-02-25 03:39:10');

-- --------------------------------------------------------

--
-- Table structure for table `larapoll_polls`
--

DROP TABLE IF EXISTS `larapoll_polls`;
CREATE TABLE IF NOT EXISTS `larapoll_polls` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maxCheck` int(11) NOT NULL DEFAULT '1',
  `canVisitorsVote` tinyint(1) NOT NULL DEFAULT '0',
  `isClosed` timestamp NULL DEFAULT NULL,
  `starts_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `canVoterSeeResult` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `larapoll_polls`
--

INSERT INTO `larapoll_polls` (`id`, `question`, `maxCheck`, `canVisitorsVote`, `isClosed`, `starts_at`, `ends_at`, `created_at`, `updated_at`, `canVoterSeeResult`) VALUES
(1, 'Startup yang diminati', 100, 1, NULL, '2019-12-31 17:00:00', '2020-12-30 17:00:00', '2020-02-25 03:39:10', '2020-02-25 03:39:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `larapoll_votes`
--

DROP TABLE IF EXISTS `larapoll_votes`;
CREATE TABLE IF NOT EXISTS `larapoll_votes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `larapoll_votes_option_id_foreign` (`option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `larapoll_votes`
--

INSERT INTO `larapoll_votes` (`id`, `user_id`, `option_id`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.2', 1, '2020-02-25 03:51:11', '2020-02-25 03:51:11'),
(2, '127.0.0.3', 2, '2020-02-25 03:59:21', '2020-02-25 03:59:21'),
(3, '127.0.0.4', 1, '2020-02-25 03:59:40', '2020-02-25 03:59:40'),
(4, '127.0.0.5', 1, '2020-02-25 04:00:07', '2020-02-25 04:00:07'),
(5, '127.0.0.1', 3, '2020-02-25 04:12:12', '2020-02-25 04:12:12');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\Speaker', 1, 'photo', '1', '1.jpg', 'image/jpeg', 'public', 44376, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 1, '2020-02-06 03:09:49', '2020-02-06 03:09:50'),
(2, 'App\\Speaker', 2, 'photo', '2', '2.jpg', 'image/jpeg', 'public', 49380, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 2, '2020-02-06 03:09:50', '2020-02-06 03:09:50'),
(3, 'App\\Speaker', 3, 'photo', '3', '3.jpg', 'image/jpeg', 'public', 14278, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 3, '2020-02-06 03:09:50', '2020-02-06 03:09:50'),
(4, 'App\\Speaker', 4, 'photo', '4', '4.jpg', 'image/jpeg', 'public', 53251, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 4, '2020-02-06 03:09:50', '2020-02-06 03:09:51'),
(5, 'App\\Speaker', 5, 'photo', '5', '5.jpg', 'image/jpeg', 'public', 30301, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 5, '2020-02-06 03:09:51', '2020-02-06 03:09:51'),
(6, 'App\\Speaker', 6, 'photo', '6', '6.jpg', 'image/jpeg', 'public', 16133, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 6, '2020-02-06 03:09:51', '2020-02-06 03:09:51'),
(7, 'App\\Venue', 1, 'photos', '1', '1.jpg', 'image/jpeg', 'public', 124351, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 7, '2020-02-06 03:09:51', '2020-02-06 03:09:52'),
(8, 'App\\Venue', 1, 'photos', '2', '2.jpg', 'image/jpeg', 'public', 47098, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 8, '2020-02-06 03:09:52', '2020-02-06 03:09:52'),
(9, 'App\\Venue', 1, 'photos', '3', '3.jpg', 'image/jpeg', 'public', 157926, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 9, '2020-02-06 03:09:52', '2020-02-06 03:09:53'),
(10, 'App\\Venue', 1, 'photos', '4', '4.jpg', 'image/jpeg', 'public', 101273, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 10, '2020-02-06 03:09:53', '2020-02-06 03:09:53'),
(11, 'App\\Venue', 1, 'photos', '5', '5.jpg', 'image/jpeg', 'public', 152944, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 11, '2020-02-06 03:09:53', '2020-02-06 03:09:53'),
(12, 'App\\Venue', 1, 'photos', '6', '6.jpg', 'image/jpeg', 'public', 134709, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 12, '2020-02-06 03:09:53', '2020-02-06 03:09:54'),
(13, 'App\\Venue', 1, 'photos', '7', '7.jpg', 'image/jpeg', 'public', 109640, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 13, '2020-02-06 03:09:54', '2020-02-06 03:09:54'),
(14, 'App\\Venue', 1, 'photos', '8', '8.jpg', 'image/jpeg', 'public', 57275, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 14, '2020-02-06 03:09:54', '2020-02-06 03:09:54'),
(15, 'App\\Hotel', 1, 'photo', '1', '1.jpg', 'image/jpeg', 'public', 176612, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 15, '2020-02-06 03:09:54', '2020-02-06 03:09:55'),
(16, 'App\\Hotel', 2, 'photo', '2', '2.jpg', 'image/jpeg', 'public', 207771, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 16, '2020-02-06 03:09:55', '2020-02-06 03:09:55'),
(17, 'App\\Hotel', 3, 'photo', '3', '3.jpg', 'image/jpeg', 'public', 164509, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 17, '2020-02-06 03:09:55', '2020-02-06 03:09:55'),
(18, 'App\\Gallery', 1, 'photos', '1', '1.jpg', 'image/jpeg', 'public', 85253, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 18, '2020-02-06 03:09:55', '2020-02-06 03:09:56'),
(19, 'App\\Gallery', 1, 'photos', '2', '2.jpg', 'image/jpeg', 'public', 107817, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 19, '2020-02-06 03:09:56', '2020-02-06 03:09:56'),
(20, 'App\\Gallery', 1, 'photos', '3', '3.jpg', 'image/jpeg', 'public', 182158, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 20, '2020-02-06 03:09:56', '2020-02-06 03:09:56'),
(21, 'App\\Gallery', 1, 'photos', '4', '4.jpg', 'image/jpeg', 'public', 112256, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 21, '2020-02-06 03:09:56', '2020-02-06 03:09:57'),
(22, 'App\\Gallery', 1, 'photos', '5', '5.jpg', 'image/jpeg', 'public', 127200, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 22, '2020-02-06 03:09:57', '2020-02-06 03:09:57'),
(23, 'App\\Gallery', 1, 'photos', '6', '6.jpg', 'image/jpeg', 'public', 57440, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 23, '2020-02-06 03:09:57', '2020-02-06 03:09:57'),
(24, 'App\\Gallery', 1, 'photos', '7', '7.jpg', 'image/jpeg', 'public', 64459, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 24, '2020-02-06 03:09:58', '2020-02-06 03:09:58'),
(25, 'App\\Gallery', 1, 'photos', '8', '8.jpg', 'image/jpeg', 'public', 88287, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 25, '2020-02-06 03:09:58', '2020-02-06 03:09:58'),
(26, 'App\\Sponsor', 1, 'logo', '1', '1.png', 'image/png', 'public', 3994, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 26, '2020-02-06 03:09:58', '2020-02-06 03:09:58'),
(27, 'App\\Sponsor', 2, 'logo', '2', '2.png', 'image/png', 'public', 2749, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 27, '2020-02-06 03:09:58', '2020-02-06 03:09:59'),
(28, 'App\\Sponsor', 3, 'logo', '3', '3.png', 'image/png', 'public', 3281, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 28, '2020-02-06 03:09:59', '2020-02-06 03:09:59'),
(29, 'App\\Sponsor', 4, 'logo', '4', '4.png', 'image/png', 'public', 2201, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 29, '2020-02-06 03:09:59', '2020-02-06 03:09:59'),
(30, 'App\\Sponsor', 5, 'logo', '5', '5.png', 'image/png', 'public', 3748, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 30, '2020-02-06 03:09:59', '2020-02-06 03:10:00'),
(31, 'App\\Sponsor', 6, 'logo', '6', '6.png', 'image/png', 'public', 2150, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 31, '2020-02-06 03:10:00', '2020-02-06 03:10:00'),
(32, 'App\\Sponsor', 7, 'logo', '7', '7.png', 'image/png', 'public', 2195, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 32, '2020-02-06 03:10:00', '2020-02-06 03:10:00'),
(33, 'App\\Sponsor', 8, 'logo', '8', '8.png', 'image/png', 'public', 1984, '[]', '{\"generated_conversions\": {\"thumb\": true}}', '[]', 33, '2020-02-06 03:10:00', '2020-02-06 03:10:00');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `ticket_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `private_reference_number` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_cancelled` tinyint(1) NOT NULL DEFAULT '0',
  `has_arrived` tinyint(1) NOT NULL DEFAULT '0',
  `arrival_time` datetime DEFAULT NULL,
  `account_id` int(10) UNSIGNED NOT NULL,
  `reference_index` int(11) NOT NULL DEFAULT '0',
  `is_refunded` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `attendees_event_id_index` (`event_id`),
  KEY `attendees_account_id_index` (`account_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `order_id`, `event_id`, `ticket_id`, `first_name`, `last_name`, `email`, `private_reference_number`, `created_at`, `updated_at`, `deleted_at`, `is_cancelled`, `has_arrived`, `arrival_time`, `account_id`, `reference_index`, `is_refunded`) VALUES
(1, 1, 1, 1, 'AAA', 'BBB', 'aaa@gmail.com', '122', '2020-02-13 01:03:33', '2020-02-13 01:03:33', NULL, 0, 0, '2020-02-13 10:00:00', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_09_24_000000_create_media_table', 1),
(8, '2019_09_24_000001_create_permissions_table', 1),
(9, '2019_09_24_000002_create_faqs_table', 1),
(10, '2019_09_24_000003_create_prices_table', 1),
(11, '2019_09_24_000004_create_users_table', 1),
(12, '2019_09_24_000005_create_amenities_table', 1),
(13, '2019_09_24_000006_create_settings_table', 1),
(14, '2019_09_24_000007_create_speakers_table', 1),
(15, '2019_09_24_000008_create_schedules_table', 1),
(16, '2019_09_24_000009_create_roles_table', 1),
(17, '2019_09_24_000010_create_venues_table', 1),
(18, '2019_09_24_000011_create_hotels_table', 1),
(19, '2019_09_24_000012_create_galleries_table', 1),
(20, '2019_09_24_000013_create_sponsors_table', 1),
(21, '2019_09_24_000014_create_amenity_price_pivot_table', 1),
(22, '2019_09_24_000015_create_role_user_pivot_table', 1),
(23, '2019_09_24_000016_create_permission_role_pivot_table', 1),
(24, '2019_09_24_000017_add_relationship_fields_to_schedules_table', 1),
(25, '2020_02_11_032456_create_categories_table', 2),
(26, '2017_01_23_115718_create_polls_table', 3),
(27, '2017_01_23_124357_create_options_table', 3),
(28, '2017_01_25_111721_create_votes_table', 3),
(29, '2019_04_20_145921_schema_changes', 3);

-- --------------------------------------------------------

--
-- Table structure for table `nominations`
--

DROP TABLE IF EXISTS `nominations`;
CREATE TABLE IF NOT EXISTS `nominations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason_for_nomination` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_nominations` int(11) DEFAULT '0',
  `is_admin_selected` tinyint(1) DEFAULT '0',
  `is_won` tinyint(1) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `panelmembers`
--

DROP TABLE IF EXISTS `panelmembers`;
CREATE TABLE IF NOT EXISTS `panelmembers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `panel_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_cancelled` tinyint(1) NOT NULL DEFAULT '0',
  `has_arrived` tinyint(1) NOT NULL DEFAULT '0',
  `arrival_time` datetime DEFAULT NULL,
  `account_id` int(10) UNSIGNED NOT NULL,
  `reference_index` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `attendees_event_id_index` (`panel_id`),
  KEY `attendees_account_id_index` (`account_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `panels`
--

DROP TABLE IF EXISTS `panels`;
CREATE TABLE IF NOT EXISTS `panels` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `account_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `seat_kuota` int(10) NOT NULL DEFAULT '0',
  `organiser_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `venue_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_address` varchar(355) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lat` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_long` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_google_place_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_share_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `social_show_facebook` tinyint(1) NOT NULL DEFAULT '1',
  `social_show_linkedin` tinyint(1) NOT NULL DEFAULT '1',
  `social_show_twitter` tinyint(1) NOT NULL DEFAULT '1',
  `social_show_email` tinyint(1) NOT NULL DEFAULT '1',
  `social_show_googleplus` tinyint(1) NOT NULL DEFAULT '1',
  `is_live` tinyint(1) NOT NULL DEFAULT '0',
  `barcode_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'QRCODE',
  `event_image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(2, 'permission_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(3, 'permission_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(4, 'permission_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(5, 'permission_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(6, 'permission_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(7, 'role_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(8, 'role_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(9, 'role_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(10, 'role_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(11, 'role_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(12, 'user_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(13, 'user_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(14, 'user_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(15, 'user_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(16, 'user_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(17, 'setting_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(18, 'setting_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(19, 'setting_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(20, 'setting_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(21, 'setting_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(22, 'speaker_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(23, 'speaker_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(24, 'speaker_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(25, 'speaker_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(26, 'speaker_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(27, 'schedule_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(28, 'schedule_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(29, 'schedule_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(30, 'schedule_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(31, 'schedule_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(32, 'venue_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(33, 'venue_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(34, 'venue_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(35, 'venue_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(36, 'venue_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(37, 'hotel_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(38, 'hotel_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(39, 'hotel_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(40, 'hotel_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(41, 'hotel_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(42, 'gallery_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(43, 'gallery_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(44, 'gallery_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(45, 'gallery_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(46, 'gallery_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(47, 'sponsor_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(48, 'sponsor_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(49, 'sponsor_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(50, 'sponsor_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(51, 'sponsor_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(52, 'faq_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(53, 'faq_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(54, 'faq_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(55, 'faq_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(56, 'faq_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(57, 'amenity_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(58, 'amenity_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(59, 'amenity_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(60, 'amenity_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(61, 'amenity_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(62, 'price_create', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(63, 'price_edit', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(64, 'price_show', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(65, 'price_delete', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(66, 'price_access', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  KEY `role_id_fk_383833` (`role_id`),
  KEY `permission_id_fk_383833` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 54),
(1, 55),
(1, 56),
(1, 57),
(1, 58),
(1, 59),
(1, 60),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 37),
(2, 38),
(2, 39),
(2, 40),
(2, 41),
(2, 42),
(2, 43),
(2, 44),
(2, 45),
(2, 46),
(2, 47),
(2, 48),
(2, 49),
(2, 50),
(2, 51),
(2, 52),
(2, 53),
(2, 54),
(2, 55),
(2, 56),
(2, 57),
(2, 58),
(2, 59),
(2, 60),
(2, 61),
(2, 62),
(2, 63),
(2, 64),
(2, 65),
(2, 66);

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

DROP TABLE IF EXISTS `prices`;
CREATE TABLE IF NOT EXISTS `prices` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `survey_id` bigint(20) NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` time DEFAULT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'free_text',
  PRIMARY KEY (`id`),
  KEY `questions_survey_id_index` (`survey_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `survey_id`, `question`, `options`, `created_at`, `updated_at`, `deleted_at`, `type`) VALUES
(1, 2, 'Siapa namamu', '[\"\"]', '2020-02-23 20:27:30', '2020-02-23 20:27:30', NULL, 'free_text'),
(2, 2, 'Berapa umurmu', '[\"\"]', '2020-02-23 20:27:30', '2020-02-23 20:27:30', NULL, 'numbers'),
(3, 2, 'Benar atau salah', '[\"Benar\",\"Salah\"]', '2020-02-23 20:27:30', '2020-02-23 20:27:30', NULL, 'single_answer'),
(4, 2, 'Berapa kali anda membaca', '[\"2\",\"3\",\"4\"]', '2020-02-23 20:27:30', '2020-02-23 20:27:30', NULL, 'multiple_answer'),
(6, 2, 'Jenis Usaha', '[\"Technology\",\" MarketPlace\",\" ShopOnline\",\" Belanja\"]', '2020-02-24 21:45:45', '2020-02-24 22:56:06', NULL, 'multiple_answer');

-- --------------------------------------------------------

--
-- Table structure for table `responses`
--

DROP TABLE IF EXISTS `responses`;
CREATE TABLE IF NOT EXISTS `responses` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question_id` bigint(20) NOT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `responses_question_id_index` (`question_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `responses`
--

INSERT INTO `responses` (`id`, `question_id`, `session_id`, `answer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '9cQNFjxh3i4MQj489mkZB1Gmff7j4gdD7QNwOOcF', 'anam', '2020-02-23 20:28:07', '2020-02-23 20:28:07', NULL),
(2, 2, '9cQNFjxh3i4MQj489mkZB1Gmff7j4gdD7QNwOOcF', '23', '2020-02-23 20:28:07', '2020-02-23 20:28:07', NULL),
(3, 3, '9cQNFjxh3i4MQj489mkZB1Gmff7j4gdD7QNwOOcF', 'Benar', '2020-02-23 20:28:07', '2020-02-23 20:28:07', NULL),
(4, 4, '9cQNFjxh3i4MQj489mkZB1Gmff7j4gdD7QNwOOcF', '3', '2020-02-23 20:28:07', '2020-02-23 20:28:07', NULL),
(5, 1, 'oJ1mCzYblqVD9eDQynRujBJIbARHzICLFkU5HyNJ', 'budi', '2020-02-23 20:34:38', '2020-02-23 20:34:38', NULL),
(6, 2, 'oJ1mCzYblqVD9eDQynRujBJIbARHzICLFkU5HyNJ', '44', '2020-02-23 20:34:38', '2020-02-23 20:34:38', NULL),
(7, 3, 'oJ1mCzYblqVD9eDQynRujBJIbARHzICLFkU5HyNJ', 'Salah', '2020-02-23 20:34:38', '2020-02-23 20:34:38', NULL),
(8, 4, 'oJ1mCzYblqVD9eDQynRujBJIbARHzICLFkU5HyNJ', '4', '2020-02-23 20:34:38', '2020-02-23 20:34:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL),
(2, 'User', '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  KEY `user_id_fk_383842` (`user_id`),
  KEY `role_id_fk_383842` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

DROP TABLE IF EXISTS `schedules`;
CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `day_number` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `speaker_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `speaker_fk_383954` (`speaker_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `day_number`, `start_time`, `title`, `subtitle`, `event_id`, `created_at`, `updated_at`, `deleted_at`, `speaker_id`) VALUES
(20, 1, '10:00:00', 'Registration', 'Registration member', 1, '2020-02-13 00:59:24', '2020-02-13 00:59:34', NULL, 1),
(21, 2, '12:00:00', 'Lunch Break', 'at Restarurant', 1, '2020-02-14 03:54:16', '2020-02-14 03:54:16', NULL, 1),
(22, 1, '11:00:00', 'Opening Speech', 'Facere provident incidunt quos voluptas.', 1, '2020-02-16 22:46:30', '2020-02-16 22:47:13', NULL, 2),
(23, 1, '13:00:00', 'Welcome Speech', 'dignissimos neque qui cum accusantium ut sit sint inventore', 1, '2020-02-16 22:46:57', '2020-02-16 22:47:24', NULL, 3),
(24, 1, '13:00:00', 'Speech 01', 'accusantium laborum nihil eos eaque accusantium aspernatur.', 1, '2020-02-16 22:47:50', '2020-02-16 22:47:50', NULL, 4),
(25, 1, '14:00:00', 'Speech 02', 'Nam ex distinctio voluptatem doloremque suscipit iusto.', 1, '2020-02-16 22:48:16', '2020-02-16 22:48:16', NULL, 1),
(26, 1, '15:00:00', 'Tea Break', 'Eligendi quo eveniet est nobis et ad temporibus odio quo.', 1, '2020-02-16 22:48:43', '2020-02-16 22:48:43', NULL, 1),
(27, 1, '16:00:00', 'Speech 03', 'Voluptatem et alias dolorum est aut sit enim neque veritatis.', 1, '2020-02-16 22:49:03', '2020-02-16 22:49:03', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'title', 'The Annual<br><span>Marketing</span> Conference', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(2, 'subtitle', '10-12 December, Downtown Conference Center, New York', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(3, 'youtube_link', 'https://www.youtube.com/watch?v=jDDaplaOz7Q', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(4, 'about_description', 'Sed nam ut dolor qui repellendus iusto odit. Possimus inventore eveniet accusamus error amet eius aut accusantium et. Non odit consequatur repudiandae sequi ea odio molestiae. Enim possimus sunt inventore in est ut optio sequi unde.', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(5, 'about_where', 'Downtown Conference Center, New York', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(6, 'about_when', 'Monday to Wednesday<br>10-12 December', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(7, 'contact_address', 'A108 Adam Street, NY 535022, USA', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(8, 'contact_phone', '+1 5589 55488 55', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(9, 'contact_email', 'info@example.com', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(10, 'footer_description', 'In alias aperiam. Placeat tempore facere. Officiis voluptate ipsam vel eveniet est dolor et totam porro. Perspiciatis ad omnis fugit molestiae recusandae possimus. Aut consectetur id quis. In inventore consequatur ad voluptate cupiditate debitis accusamus repellat cumque.', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(11, 'footer_address', 'A108 Adam Street <br> New York, NY 535022<br> United States ', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(12, 'footer_twitter', '#', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(13, 'footer_facebook', '#', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(14, 'footer_instagram', '#', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(15, 'footer_googleplus', '#', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL),
(16, 'footer_linkedin', '#', '2020-02-06 03:09:49', '2020-02-06 03:09:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `speakers`
--

DROP TABLE IF EXISTS `speakers`;
CREATE TABLE IF NOT EXISTS `speakers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_description` longtext COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `speakers`
--

INSERT INTO `speakers` (`id`, `name`, `description`, `twitter`, `facebook`, `linkedin`, `full_description`, `photo`, `event_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Tommy Tjokro', 'Public Speaker', 'tommy.tjokro', 'tommy.tjokro', 'tommy.tjokro', 'tommy.tjokro', 'http://localhost:8000/storage/files/1/speakers/team-3.jpg', 1, '2020-02-13 00:11:15', '2020-02-17 02:34:31', NULL),
(1, 'Jane Parker', 'Sr. Software Engineer', 'jane.parker', 'jane.parker', 'jane.parker', NULL, 'http://localhost:8000/storage/files/1/speakers/team-5.jpg', 1, '2020-02-14 03:48:40', '2020-02-17 02:34:49', NULL),
(3, 'Cole Emmerich', 'Consequuntur odio aut', 'Consequuntur', 'Consequuntur', 'Consequuntur', NULL, 'http://localhost:8000/storage/files/1/speakers/team-8.jpg', 1, '2020-02-17 02:35:46', '2020-02-17 02:35:46', NULL),
(4, 'Jack Christiansen', 'Jack.Christiansen', 'Jack.Christiansen', 'Jack.Christiansen', 'Jack.Christiansen', NULL, 'http://localhost:8000/storage/files/1/speakers/team-6.jpg', 1, '2020-02-17 02:36:33', '2020-02-17 02:36:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE IF NOT EXISTS `sponsors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`id`, `name`, `link`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Strider', '#', '2020-02-06 03:09:58', '2020-02-06 03:09:58', NULL),
(2, 'Runtastic', '#', '2020-02-06 03:09:58', '2020-02-06 03:09:58', NULL),
(3, 'EditShare', '#', '2020-02-06 03:09:59', '2020-02-06 03:09:59', NULL),
(4, 'InFocus', '#', '2020-02-06 03:09:59', '2020-02-06 03:09:59', NULL),
(5, 'gategroup', '#', '2020-02-06 03:09:59', '2020-02-06 03:09:59', NULL),
(6, 'Cadent', '#', '2020-02-06 03:10:00', '2020-02-06 03:10:00', NULL),
(7, 'Ceph', '#', '2020-02-06 03:10:00', '2020-02-06 03:10:00', NULL),
(8, 'Alitalia', '#', '2020-02-06 03:10:00', '2020-02-06 03:10:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
CREATE TABLE IF NOT EXISTS `surveys` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `surveys_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `title`, `user_id`, `is_published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Survey 001', 1, 0, '2020-02-23 20:27:30', '2020-02-23 20:27:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(14) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `name`, `price`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Regular', 125, '<div>Regular Seating\r\n</div><div>Coffee Break\r\n</div><div>Custom Badge\r\n</div>', '2020-02-20 02:24:41', '2020-02-20 02:39:17', NULL),
(2, 'Special', 250, '<div>Regular Seating\r\n</div><div>Coffee Break\r\n</div><div>Custom Badge\r\n</div><div>Community Access\r\n</div>', '2020-02-20 02:25:02', '2020-02-20 02:39:24', NULL),
(3, 'Executive', 500, '<div>Regular Seating\r\n</div><div>Coffee Break\r\n</div><div>Lunch\r\n</div><div>Custom Badge\r\n</div><div>Community Access\r\n</div><div>Workshop Access\r\n</div><div>After Party\r\n</div><div>Meet the Speakers\r\n</div><div>Papers\r\n</div>', '2020-02-20 02:25:17', '2020-02-20 02:44:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin@admin.com', '2020-02-01 00:00:00', '$2y$10$7Xn/8bPJ89ypj0cIxwoH9OOXnbK/.9xrLfFh2G4LUSRkw6j7Agn0K', NULL, '2019-09-24 12:16:02', '2019-09-24 12:16:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

DROP TABLE IF EXISTS `venues`;
CREATE TABLE IF NOT EXISTS `venues` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `name`, `address`, `latitude`, `longitude`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Downtown Conference Center, New York', '157 William St, New York, NY 10038', '40.7101282', '-74.0062269', 'Iste nobis eum sapiente sunt enim dolores labore accusantium autem. Cumque beatae ipsam. Est quae sit qui voluptatem corporis velit. Qui maxime accusamus possimus. Consequatur sequi et ea suscipit enim nesciunt quia velit.', '2020-02-06 03:09:51', '2020-02-06 03:09:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `votings`
--

DROP TABLE IF EXISTS `votings`;
CREATE TABLE IF NOT EXISTS `votings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nomination_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
