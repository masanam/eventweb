<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Nomination;
use Faker\Generator as Faker;

$factory->define(Nomination::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'image' => $faker->word,
        'business_name' => $faker->word,
        'reason_for_nomination' => $faker->word,
        'no_of_nominations' => $faker->randomDigitNotNull,
        'is_admin_selected' => $faker->word,
        'is_won' => $faker->word,
        'user_id' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
