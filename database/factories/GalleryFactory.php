<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Gallery;
use Faker\Generator as Faker;

$factory->define(Gallery::class, function (Faker $faker) {

    return [
        'album_id' => $faker->randomDigitNotNull,
        'title' => $faker->word,
        'content' => $faker->text,
        'picture' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
