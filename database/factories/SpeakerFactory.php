<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Speaker;
use Faker\Generator as Faker;

$factory->define(Speaker::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'description' => $faker->text,
        'twitter' => $faker->word,
        'facebook' => $faker->word,
        'linkedin' => $faker->word,
        'full_description' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
