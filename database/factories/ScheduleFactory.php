<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Schedule;
use Faker\Generator as Faker;

$factory->define(Schedule::class, function (Faker $faker) {

    return [
        'day_number' => $faker->randomDigitNotNull,
        'start_time' => $faker->word,
        'title' => $faker->word,
        'subtitle' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'speaker_id' => $faker->randomDigitNotNull
    ];
});
