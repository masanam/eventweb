<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Hotel;
use Faker\Generator as Faker;

$factory->define(Hotel::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'address' => $faker->word,
        'description' => $faker->text,
        'rating' => $faker->randomDigitNotNull,
        'facility' => $faker->text,
        'resto' => $faker->text,
        'image' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
