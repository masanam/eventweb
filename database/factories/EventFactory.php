<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'location' => $faker->word,
        'description' => $faker->text,
        'start_date' => $faker->date('Y-m-d H:i:s'),
        'end_date' => $faker->date('Y-m-d H:i:s'),
        'sale_date' => $faker->date('Y-m-d H:i:s'),
        'user_id' => $faker->randomDigitNotNull,
        'currency_id' => $faker->randomDigitNotNull,
        'sales_volume' => $faker->randomDigitNotNull,
        'organiser_id' => $faker->randomDigitNotNull,
        'venue_name' => $faker->word,
        'venue_name_full' => $faker->word,
        'location_address' => $faker->word,
        'location_country' => $faker->word,
        'location_state' => $faker->word,
        'location_post_code' => $faker->word,
        'location_lat' => $faker->word,
        'location_long' => $faker->word,
        'location_google_place_id' => $faker->word,
        'pre_order_display_message' => $faker->text,
        'post_order_display_message' => $faker->text,
        'social_share_text' => $faker->text,
        'social_show_facebook' => $faker->word,
        'social_show_linkedin' => $faker->word,
        'social_show_twitter' => $faker->word,
        'social_show_email' => $faker->word,
        'social_show_googleplus' => $faker->word,
        'location_is_manual' => $faker->randomDigitNotNull,
        'is_live' => $faker->word,
        'barcode_type' => $faker->word,
        'event_image' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
