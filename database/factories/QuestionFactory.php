<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {

    return [
        'question' => $faker->word,
        'type' => $faker->word,
        'options' => $faker->text,
        'category_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
