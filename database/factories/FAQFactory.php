<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\FAQ;
use Faker\Generator as Faker;

$factory->define(FAQ::class, function (Faker $faker) {

    return [
        'question' => $faker->word,
        'answer' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
