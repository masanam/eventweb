<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Attendee;
use Faker\Generator as Faker;

$factory->define(Attendee::class, function (Faker $faker) {

    return [
        'order_id' => $faker->randomDigitNotNull,
        'event_id' => $faker->randomDigitNotNull,
        'ticket_id' => $faker->randomDigitNotNull,
        'first_name' => $faker->word,
        'last_name' => $faker->word,
        'email' => $faker->word,
        'private_reference_number' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'is_cancelled' => $faker->word,
        'has_arrived' => $faker->word,
        'arrival_time' => $faker->date('Y-m-d H:i:s'),
        'account_id' => $faker->randomDigitNotNull,
        'reference_index' => $faker->randomDigitNotNull,
        'is_refunded' => $faker->word
    ];
});
