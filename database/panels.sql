-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Mar 2020 pada 04.37
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventcore`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `panels`
--

CREATE TABLE `panels` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `account_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `seat_kuota` int(10) NOT NULL DEFAULT 0,
  `organiser_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `venue_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_address` varchar(355) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lat` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_long` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_google_place_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_share_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_show_facebook` tinyint(1) NOT NULL DEFAULT 1,
  `social_show_linkedin` tinyint(1) NOT NULL DEFAULT 1,
  `social_show_twitter` tinyint(1) NOT NULL DEFAULT 1,
  `social_show_email` tinyint(1) NOT NULL DEFAULT 1,
  `social_show_googleplus` tinyint(1) NOT NULL DEFAULT 1,
  `is_live` tinyint(1) NOT NULL DEFAULT 0,
  `barcode_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'QRCODE',
  `event_image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `panels`
--

INSERT INTO `panels` (`id`, `title`, `location`, `description`, `start_date`, `end_date`, `start_time`, `end_time`, `account_id`, `user_id`, `seat_kuota`, `organiser_id`, `venue_name`, `location_address`, `location_lat`, `location_long`, `location_google_place_id`, `social_share_text`, `social_show_facebook`, `social_show_linkedin`, `social_show_twitter`, `social_show_email`, `social_show_googleplus`, `is_live`, `barcode_type`, `event_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Gethering Corona Virus', 'Depok', 'Bagi bagi masker gratis', '2020-03-05', '2020-03-06', '00:00:00', '00:00:00', 1, 1, 5, 1, 'Rumah Sakit', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 0, 'QRCODE', NULL, '2020-03-04 23:52:23', '2020-03-04 23:52:23', NULL),
(4, 'Test', 'Test', 'Test', '2020-03-05', '2020-03-06', '14:42:00', '15:42:00', 1, 1, 2, 1, 'Test', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 0, 'QRCODE', NULL, '2020-03-05 00:43:23', '2020-03-05 00:43:23', NULL),
(5, 'Test 2', 'JakSel', 'Test 2', '2020-03-05', '2020-03-27', '17:05:00', '18:05:00', 1, 1, 5, 1, 'Kemang', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 0, 'QRCODE', NULL, '2020-03-05 03:05:23', '2020-03-05 03:05:23', NULL),
(6, 'Test 3', 'JakSel', 'Test 3', '2020-03-10', '2020-03-12', '17:06:00', '19:06:00', 1, 1, 2, 1, 'Blok M', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 0, 'QRCODE', NULL, '2020-03-05 03:06:34', '2020-03-05 03:06:34', NULL),
(7, 'Test 4', 'Test 4', 'Test 4', '2020-03-06', '2020-03-07', '17:18:00', '18:18:00', 1, 1, 2, 1, 'Test 4', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 0, 'QRCODE', NULL, '2020-03-05 03:18:14', '2020-03-05 03:18:14', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `panels`
--
ALTER TABLE `panels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `events_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `panels`
--
ALTER TABLE `panels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
